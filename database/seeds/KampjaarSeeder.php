<?php

use Illuminate\Database\Seeder;

class KampjaarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('HUplicatie\Kampjaar')->create([
            'jaar'              => 2019,
            'start'             => '2019-08-26',
            'eind'              => '2019-08-30',
            'actief'            => 1,
            'open'              => 1,
            'inschrijfbedrag'   => 100.00,
            'verzekeringbedrag' => 5.00,
            'logo'              => null,
        ]);
    }
}
