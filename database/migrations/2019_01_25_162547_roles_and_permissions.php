<?php

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Collection::make([
            Roles::WEBMEESTER,
            Roles::STAFFER,
            Roles::MEDEWERKER,
            Roles::INSCHRIJVING,
        ])->each(function ($name) {
            Role::create(['name' => $name]);
        });

        Permission::create(['name' => 'View Dashboard']);
        Role::findByName(Roles::STAFFER)->givePermissionTo('View Dashboard');

        User::find(1)->assignRole(Roles::WEBMEESTER);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::find(1)->removeRole(Roles::WEBMEESTER);
        Role::whereIn('name', [
            Roles::WEBMEESTER,
            Roles::STAFFER,
            Roles::MEDEWERKER,
            Roles::INSCHRIJVING,
        ])->delete();
    }
}
