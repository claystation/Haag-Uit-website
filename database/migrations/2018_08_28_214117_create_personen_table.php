<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('personen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voornaam');
            $table->string('roepnaam');
            $table->string('voorletters');
            $table->string('achternaam');
            $table->string('straat');
            $table->string('huisnummer');
            $table->string('postcode');
            $table->string('woonplaats');
            $table->date('geboortedatum');
            $table->string('opleiding');
            $table->string('geslacht');
            $table->string('email');
            $table->string('mobiel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('personen');
    }
}
