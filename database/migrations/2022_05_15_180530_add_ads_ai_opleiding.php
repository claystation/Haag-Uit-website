<?php

use HUplicatie\Studie;
use Illuminate\Database\Migrations\Migration;

class AddAdsAiOpleiding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Studie::create(['naam' => 'ADS&AI']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Studie::where(['naam' => 'ADS&AI']);
    }
}
