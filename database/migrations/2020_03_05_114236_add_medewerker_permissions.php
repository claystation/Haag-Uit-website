<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddMedewerkerPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Medewerkers']);
        Permission::create(['name' => 'Create Medewerker']);
        Permission::create(['name' => 'Edit Medewerker']);
        Permission::create(['name' => 'Delete Medewerker']);

        Role::findByName(Roles::STAFFER)->givePermissionTo([
            'View Medewerkers',
            'Create Medewerker',
            'Edit Medewerker',
            'Delete Medewerker',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', [
            'View Medewerkers',
            'Create Medewerker',
            'Edit Medewerker',
            'Delete Medewerker',
        ])->delete();
    }
}
