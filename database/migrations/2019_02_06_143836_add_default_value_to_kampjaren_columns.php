<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueToKampjarenColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kampjaren', function (Blueprint $table) {
            $table->boolean('actief')->default(0)->change();
            $table->boolean('open')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kampjaren', function (Blueprint $table) {
            $table->boolean('actief')->change();
            $table->boolean('open')->change();
        });
    }
}
