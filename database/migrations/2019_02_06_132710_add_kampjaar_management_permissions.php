<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddKampjaarManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Kampjaren']);
        Permission::create(['name' => 'View Kampjaar']);
        Permission::create(['name' => 'Create Kampjaar']);
        Permission::create(['name' => 'Edit Kampjaar']);
        Permission::create(['name' => 'Delete Kampjaar']);

        Role::findByName(Roles::STAFFER)->givePermissionTo('View Kampjaren', 'View Kampjaar', 'Create Kampjaar', 'Edit Kampjaar', 'Delete Kampjaar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', ['View Kampjaren', 'View Kampjaar', 'Create Kampjaar', 'Edit Kampjaar', 'Delete Kampjaar'])->delete();
    }
}
