<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeRoepnaamVoorlettersNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personen', function (Blueprint $table) {
            $table->string('roepnaam')->nullable()->change();
            $table->string('voorletters')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personen', function (Blueprint $table) {
            $table->string('roepnaam')->change();
            $table->string('voorletters')->change();
        });
    }
}
