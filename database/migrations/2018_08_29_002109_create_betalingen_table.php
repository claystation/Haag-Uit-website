<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetalingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betalingen', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['ideal', 'incasso']);
            $table->string('transactie_id');
            $table->decimal('bedrag');
            $table->string('status')->default('open');
            $table->string('rekeninghouder_naam')->nullable();
            $table->string('rekeninghouder_nummer')->nullable();
            $table->string('rekeninghouder_woonplaats')->nullable();
            $table->integer('kamp_inschrijving_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('betalingen');
    }
}
