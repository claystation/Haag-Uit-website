<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddPersonenPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Personen']);
        Permission::create(['name' => 'View Persoon']);
        Permission::create(['name' => 'Edit Persoon']);
        Permission::create(['name' => 'Delete Persoon']);

        Role::findByName(Roles::STAFFER)->givePermissionTo('View Personen', 'View Persoon', 'Edit Persoon', 'Delete Persoon');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', ['View Personen', 'View Persoon', 'Edit Persoon', 'Delete Persoon'])->delete();
    }
}
