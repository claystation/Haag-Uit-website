<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddInschrijvingManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Inschrijvingen']);
        Permission::create(['name' => 'View Inschrijving']);
        Permission::create(['name' => 'Create Inschrijving']);
        Permission::create(['name' => 'Edit Inschrijving']);
        Permission::create(['name' => 'Delete Inschrijving']);

        Role::findByName(Roles::STAFFER)->givePermissionTo('View Inschrijvingen', 'View Inschrijving', 'Create Inschrijving', 'Edit Inschrijving', 'Delete Inschrijving');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', ['View Inschrijvingen', 'View Inschrijving', 'Create Inschrijving', 'Edit Inschrijving', 'Delete Inschrijving'])->delete();
    }
}
