<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedewerkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medewerkers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('telefoon_nood');
            $table->text('studentnummer')->nullable();
            $table->text('opmerking')->nullable();
            $table->text('ervaring')->nullable();
            $table->text('motivatie')->nullable();
            $table->text('eigenschappen')->nullable();
            $table->string('ehbo')->default('Nee');
            $table->string('rijbewijs')->default('Nee');
            $table->year('jaar');
            $table->bigInteger('persoon_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medewerkers');
    }
}
