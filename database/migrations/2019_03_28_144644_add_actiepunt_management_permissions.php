<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddActiepuntManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Actiepunten']);
        Permission::create(['name' => 'View Actiepunt']);
        Permission::create(['name' => 'Create Actiepunt']);
        Permission::create(['name' => 'Edit Actiepunt']);
        Permission::create(['name' => 'Delete Actiepunt']);

        Role::findByName(Roles::STAFFER)->givePermissionTo([
            'View Actiepunten',
            'View Actiepunt',
            'Create Actiepunt',
            'Edit Actiepunt',
            'Delete Actiepunt',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', [
            'View Actiepunten',
            'View Actiepunt',
            'Create Actiepunt',
            'Edit Actiepunt',
            'Delete Actiepunt',
        ])->delete();
    }
}
