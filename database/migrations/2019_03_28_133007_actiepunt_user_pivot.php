<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActiepuntUserPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actiepunt_user', function (Blueprint $table) {
            $table->unsignedInteger('actiepunt_id');
            $table->unsignedInteger('user_id');

            $table->unique(['actiepunt_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actiepunt_user');
    }
}
