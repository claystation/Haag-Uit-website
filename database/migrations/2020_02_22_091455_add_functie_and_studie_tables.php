<?php

use HUplicatie\Functie;
use HUplicatie\Studie;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFunctieAndStudieTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('inschrijfbaar')->default(true);
            $table->string('naam');
            $table->timestamps();
        });

        Schema::create('functies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naam');
            $table->timestamps();
        });

        Functie::create(['naam' => 'Leider']);
        Functie::create(['naam' => 'BAR']);
        Functie::create(['naam' => 'Keuken']);
        Functie::create(['naam' => 'Logistiek']);
        Functie::create(['naam' => 'Foto']);
        Functie::create(['naam' => 'Video']);
        Functie::create(['naam' => 'EHBO']);
        Functie::create(['naam' => 'Techniek']);

        Studie::create(['naam' => 'HBO-ICT']);
        Studie::create(['naam' => 'CMD']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('functies');
        Schema::dropIfExists('studies');
    }
}
