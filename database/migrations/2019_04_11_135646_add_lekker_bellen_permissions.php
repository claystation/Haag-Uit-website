<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddLekkerBellenPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Lekkerbellen']);
        Role::findByName(Roles::STAFFER)->givePermissionTo('View Lekkerbellen');
        Role::findByName(Roles::MEDEWERKER)->givePermissionTo('View Lekkerbellen');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'View Lekkerbellen')->delete();
    }
}
