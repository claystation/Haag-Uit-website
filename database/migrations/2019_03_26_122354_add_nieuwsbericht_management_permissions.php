<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddNieuwsberichtManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Nieuwsberichten']);
        Permission::create(['name' => 'View Nieuwsbericht']);
        Permission::create(['name' => 'Create Nieuwsbericht']);
        Permission::create(['name' => 'Edit Nieuwsbericht']);
        Permission::create(['name' => 'Delete Nieuwsbericht']);

        Role::findByName(Roles::STAFFER)->givePermissionTo([
            'View Nieuwsberichten',
            'View Nieuwsbericht',
            'Create Nieuwsbericht',
            'Edit Nieuwsbericht',
            'Delete Nieuwsbericht',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', [
            'View Nieuwsberichten',
            'View Nieuwsbericht',
            'Create Nieuwsbericht',
            'Edit Nieuwsbericht',
            'Delete Nieuwsbericht',
        ])->delete();
    }
}
