<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAanwezigheidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aanwezigheden', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('medewerker_id');
            $table->integer('meeting');
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('medewerker_id')
                ->references('id')
                ->on('medewerkers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aanwezigheden');
    }
}
