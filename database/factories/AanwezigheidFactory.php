<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use HUplicatie\Aanwezigheid;
use HUplicatie\Medewerker;

$factory->define(Aanwezigheid::class, function (Faker $faker) {
    return [
        'meeting'       => $faker->numberBetween(0, 4),
        'status'        => $faker->numberBetween(0, 2),
        'medewerker_id' => function () {
            return factory(Medewerker::class)->create()->id;
        },
    ];
});
