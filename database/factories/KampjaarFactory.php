<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(HUplicatie\Kampjaar::class, function (Faker $faker) {
    $date = Carbon::createFromFormat('Y-m-d', $faker->date());

    return [
        'jaar'              => $date->year,
        'start'             => $date->toDateString(),
        'eind'              => $date->addDay(7)->toDateString(),
        'actief'            => 0,
        'open'              => 0,
        'inschrijfbedrag'   => 100.00,
        'verzekeringbedrag' => 5.00,
    ];
});
