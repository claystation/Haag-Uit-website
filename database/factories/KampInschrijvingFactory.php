<?php

use Faker\Generator as Faker;
use HUplicatie\Kampjaar;
use HUplicatie\Persoon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(HUplicatie\KampInschrijving::class, function (Faker $faker) {
    return [
        'kledingmaat'   => $faker->randomElement(['S', 'M', 'L']),
        'telefoon_nood' => $faker->phoneNumber,
        'opmerking'     => $faker->text,
        'persoon_id'    => function () {
            return factory(Persoon::class)->create()->id;
        },
        'jaar'          => function () {
            return factory(Kampjaar::class)->create()->jaar;
        },
        'annuleringsverzekering' => $faker->boolean,
    ];
});
