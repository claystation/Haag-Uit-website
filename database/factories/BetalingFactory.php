<?php

use Faker\Generator as Faker;
use HUplicatie\KampInschrijving;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(HUplicatie\Betaling::class, function (Faker $faker) {
    $type = $faker->randomElement(['ideal', 'incasso']);

    return [
        'type'                      => $type,
        'transactie_id'             => $type === 'ideal' ? $faker->regexify('tr_[A-Za-z0-9]{10}') : $faker->randomNumber(6),
        'bedrag'                    => (string) $faker->randomFloat(2, 1, 100),
        'status'                    => 'open',
        'rekeninghouder_naam'       => $faker->name,
        'rekeninghouder_nummer'     => $faker->bankAccountNumber,
        'rekeninghouder_woonplaats' => $faker->city,
        'kamp_inschrijving_id'      => function () {
            return factory(KampInschrijving::class)->create()->id;
        },
    ];
});
