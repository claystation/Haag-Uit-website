<?php

use Faker\Generator as Faker;
use HUplicatie\Kampjaar;
use HUplicatie\Persoon;
use HUplicatie\Studie;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(HUplicatie\Medewerker::class, function (Faker $faker) {
    return [
        'telefoon_nood' => $faker->phoneNumber,
        'opmerking'     => $faker->text,
        'ervaring'      => $faker->paragraph,
        'motivatie'     => $faker->paragraph,
        'eigenschappen' => $faker->paragraph,
        'ehbo'          => $faker->randomElement(['EHBO', 'BHV', 'Nee']),
        'rijbewijs'     => $faker->randomElement(['B', 'E', 'Geen']),
        'studentnummer' => $faker->randomNumber(8, true),
        'jaar'          => function () {
            return factory(Kampjaar::class)->create()->jaar;
        },
        'persoon_id'    => function () {
            return factory(Persoon::class)->create()->id;
        },
    ];
});
