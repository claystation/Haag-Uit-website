<?php

use Faker\Generator as Faker;

$factory->define(HUplicatie\Quote::class, function (Faker $faker) {
    return [
        'tekst' => $faker->sentence,
        'naam'  => $faker->name,
        'jaar'  => $faker->year,
    ];
});
