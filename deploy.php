<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'vendor/deployer/recipes/recipe/slack.php';

// Project name
set('application', 'HUplicatie');

// Project repository
set('repository', 'git@gitlab.com:claystation/Haag-Uit-website.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);
set('default_stage', 'staging');

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

set('slack_webhook', 'https://hooks.slack.com/services/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');
set('slack_text', 'Deploying `{{branch}}` to *{{target}}*');
set('slack_success_text', 'Deploy of `{{branch}}` to *{{target}}* was successful');
set('slack_failure_text', 'Deploy of `{{branch}}` to *{{target}}* failed!');
set('slack_success_color', 'good');
set('slack_failure_color', 'danger');

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts
host('staging')
    ->stage('staging')
    ->hostname('kip6.haaguit.com')
    ->port(47538)
    ->user('haaguit')
    ->set('deploy_path', '/var/www/kip6.haaguit.com')
    ->set('branch', 'master');

host('production')
    ->stage('prod')
    ->hostname('kip6.haaguit.com')
    ->port(47538)
    ->user('haaguit')
    ->set('deploy_path', '/var/www/haaguit.com');

task('release', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:version:absorb',
    'artisan:config:cache',
    'artisan:optimize',
    'artisan:migrate',
    'artisan:queue:restart',
    'deploy:symlink',
    'deploy:unlock',
]);

task('deploy', [
    'release',
    'cleanup',
    'success',
]);

task('artisan:version:absorb', function () {
    run('{{bin/php}} {{release_path}}/artisan version:absorb');
});

task('upload', function () {
    upload(__DIR__.'/', '{{release_path}}');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
after('deploy:failed', 'slack:notify:failure');
