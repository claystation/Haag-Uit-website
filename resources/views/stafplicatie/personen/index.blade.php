@extends('stafplicatie.layouts.app')

@section('content')
    <entities-table :entities="{{ json_encode($personen) }}" inline-template>
        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex">
                                    <div class="p-2">
                                        <h5 class="card-title">Personen</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="d-table-cell" scope="col">Naam</th>
                                        <th class="d-none d-md-table-cell" scope="col">Email</th>
                                        <th class="d-table-cell" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="persoon-table-row" v-for="(persoon, index) in items" :key="persoon.id"
                                                            :persoon="persoon"
                                                            @deleted="remove(index)">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </entities-table>
@endsection