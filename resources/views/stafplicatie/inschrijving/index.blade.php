@extends('stafplicatie.layouts.app')

@section('content')
    <div class="py-4">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1>{{ $kampjaar->jaar }}</h1>
                <div class="d-flex align-items-center">
                    <a class="btn btn-primary" href="/stafplicatie/api/inschrijving/export">Export alles</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <inschrijvingen></inschrijvingen>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
