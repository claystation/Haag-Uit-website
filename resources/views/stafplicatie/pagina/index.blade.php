@extends('stafplicatie.layouts.app')

@section('content')
        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="p-2">
                                    <h5 class="card-title">Paginas</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="d-table-cell" scope="col">Naam</th>
                                        <th class="d-none d-md-table-cell" scope="col">Titel</th>
                                        <th class="d-table-cell" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($paginas as $pagina)
                                    <tr>
                                        <th class="align-middle d-table-cell" scope="row">{{ $pagina->naam }}</th>
                                        <td class="align-middle d-none d-md-table-cell"> {{ $pagina->titel }}</td>
                                        <td class="">
                                            <a role="button" class="btn btn-primary"
                                               href="/stafplicatie/pagina/{{ $pagina->id }}/edit"><span
                                                        class="fas fa-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection