<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('/assets/stafplicatie/css/stafplicatie.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="c-app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            @yield('auth')
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ mix('/assets/stafplicatie/js/manifest.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/vendor.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/stafplicatie.js') }}" defer></script>
</body>
</html>
