<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Stafplicatie</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('/assets/stafplicatie/css/stafplicatie.css') }}" rel="stylesheet">
    <link href="{{ mix('/assets/stafplicatie/css/free.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/assets/stafplicatie/css/brand.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/assets/stafplicatie/css/flag.min.css') }}" rel="stylesheet">

    <script>
        window.app = {!! json_encode([
            'signedIn' => Auth::check(),
            'user' => Auth::user()
        ]); !!}
    </script>
</head>
<body class="c-app">
<div id="app" class="flex-fill">
        @include('stafplicatie.layouts.sidebar')
        <div class="c-wrapper">
            @include('stafplicatie.layouts.header')
            <div class="c-body">
                <main class="c-main">
                    <div class="container-fluid">
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        @yield('content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <flash message="{{ session('flash') }}"></flash>
            <new-inschrijving></new-inschrijving>
            @include('stafplicatie.layouts.footer')
        </div>
</div>

<!-- Scripts -->
<script src="{{ mix('/assets/stafplicatie/js/manifest.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/vendor.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/stafplicatie.js') }}" defer></script>
</body>
</html>
