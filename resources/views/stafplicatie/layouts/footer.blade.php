<footer class="c-footer">
    <div>
        <span>&copy; {{ Carbon\Carbon::now()->year }} Haag Uit</span>
        <span> | Developed by: <a href="mailto:huisicter@haaguit.com">Huis ICT'er</a></span>
        <span> | @version</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://www.blackgate.nl">BlackGATE</a>
    </div>
</footer>