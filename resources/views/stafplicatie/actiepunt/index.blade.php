@extends('stafplicatie.layouts.app')

@section('content')
    <actiepunt-page :actiepunten="{{ json_encode($actiepunten) }}" inline-template>
        <div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Actiepunten</h3>
                                <actiepunt-creation-modal @added="addToCollection"></actiepunt-creation-modal>
                            </div>
                            <div class="d-flex justify-content-around mb-1">
                                <div class="btn-group btn-group-sm" role="group" aria-label="Staffers">
                                    <button type="button" v-for="staffer in assignableUsers" class="btn btn-sm" :class="isFiltered(staffer.id)"
                                            v-text="staffer.name" @click="filterUser(staffer.id)"></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="d-table-cell">Beschrijving</th>
                                            <th class="d-none d-md-table-cell">Wie</th>
                                            <th class="d-none d-lg-table-cell">Aangemaakt</th>
                                            <th class="d-none d-lg-table-cell">Deadline</th>
                                            <th class="d-table-cell"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr is="actiepunt-table-row" v-for="(ap, index) in openActiepunten" :key="ap.id"
                                            :actiepunt="ap"></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-end">
                                    <a class="btn btn-primary" role="button" @click="toggleCompletedAptjes()" href="#">
                                        Afgemaakte APtjes
                                    </a>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="collapse" id="completedAptjes">
                                        <div class="card">
                                            <div class="card-body">
                                                <h3 class="card-title">Afgemaakte actiepunten</h3>
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Beschrijving</th>
                                                        <th>Wie</th>
                                                        <th>Aangemaakt</th>
                                                        <th>Deadline</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr is="actiepunt-table-row"
                                                        v-for="(ap, index) in completedActiepunten"
                                                        :key="ap.id"
                                                        :actiepunt="ap"></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <actiepunt-view-edit-modal :assignable-users="assignableUsers"></actiepunt-view-edit-modal>
            </div>
        </div>
    </actiepunt-page>
@endsection