@extends('stafplicatie.layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Sponsor aanmaken</h5>
                    <form action="/stafplicatie/sponsor" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="naam">Naam</label>
                            <input type="text" class="form-control {{ $errors->has('naam') ? 'is-invalid' : '' }}"
                                   id="naam" name="naam" value="{{ old('naam') }}">
                            @if ($errors->has('naam'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('naam') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="url">Url</label>
                            <input type="text" class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}"
                                   id="url" name="url" value="{{ old('url') }}">
                            @if ($errors->has('url'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('url') }}
                                </div>
                            @endif
                        </div>
                        <image-upload error="{{ $errors->first('logo') }}"></image-upload>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Aanmaken</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection