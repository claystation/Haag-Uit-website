@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Sponsor wijzigen</h5>
                    <form action="/stafplicatie/sponsor/{{ $sponsor->id }}" method="post"
                          enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="naam">Naam</label>
                            <input type="text" class="form-control  {{ $errors->has('naam') ? 'is-invalid' : '' }}"
                                   id="naam" name="naam" value="{{ old('naam', $sponsor->naam) }}">
                            @if ($errors->has('naam'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('naam') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="url">Url</label>
                            <input type="text" class="form-control  {{ $errors->has('url') ? 'is-invalid' : '' }}"
                                   id="url" name="url" value="{{ old('url', $sponsor->url) }}" >
                            @if ($errors->has('url'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('url') }}
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Huidige logo</h5>
                                        <img src="{{$sponsor->logo}}" alt="Sponsor logo van {{ $sponsor->naam }}"
                                             style="max-height:270px; max-width: 270px" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <image-upload error="{{ $errors->first('logo') }}"></image-upload>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Wijzigen</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection