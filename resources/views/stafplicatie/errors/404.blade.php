@extends('stafplicatie.errors.base')

@section('error')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-4">404</h1>
                <h4 class="pt-3">EHBO, we have a problem!</h4>
                <p class="text-muted">Deze pagina is zoek!</p>
            </div>
            <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg btn-block" role="button"
               aria-disabled="true">Terug naar waar je vandaan kwam</a>
        </div>
    </div>
@endsection