@extends('stafplicatie.errors.base')

@section('error')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-4">500</h1>
                <h4 class="pt-3">BAR, we have a problem!</h4>
                <p class="text-muted">De server zit zonder bier!</p>
            </div>
            <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg btn-block" role="button"
               aria-disabled="true">Terug naar waar je vandaan kwam</a>
        </div>
    </div>
@endsection