<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Stafplicatie</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('/assets/stafplicatie/css/stafplicatie.css') }}" rel="stylesheet">
</head>
<body class="c-app flex-row align-items-center">
<div id="app" class="container">
    @yield('error')
</div>

<!-- Scripts -->
<script src="{{ mix('/assets/stafplicatie/js/manifest.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/vendor.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/stafplicatie.js') }}" defer></script>
</body>
</html>