@extends('stafplicatie.layouts.app')

@section('content')
    <entities-table :entities="{{ json_encode($nieuwsberichten) }}" inline-template>
        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex">
                                    <div class="p-2">
                                        <h5 class="card-title">Nieuwsberichten</h5>
                                    </div>
                                    <div class="ml-auto p-2">
                                        <a role="button" class="btn btn-success"
                                           href="/stafplicatie/nieuwsbericht/create">
                                            <i class="fas fa-pen"></i> Nieuwe publiceren
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="d-table-cell" scope="col">Id</th>
                                        <th class="d-none d-md-table-cell" scope="col">Titel</th>
                                        <th class="d-none d-md-table-cell" scope="col">Gepubliceerd op</th>
                                        <th class="d-table-cell" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr is="nieuwsbericht-table-row" v-for="(bericht, index) in items" :key="bericht.id"
                                        :bericht="bericht" @deleted="remove(index)">
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                {{ $nieuwsberichten->render() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </entities-table>
@endsection