@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Nieuwsbericht publiceren</h5>
                    <form action="/stafplicatie/nieuwsbericht" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="titel">Titel</label>
                            <input type="text" class="form-control {{ $errors->has('titel') ? 'is-invalid' : '' }}"
                                   id="titel" name="titel" value="{{ old('titel') }}"
                                   placeholder="Titel">
                            @if ($errors->has('titel'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('titel') }}
                                </div>
                            @endif
                        </div>
                        <markdown-preview :old-tekst="{{ json_encode(old('tekst', '')) }}" inline-template>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tekst">Tekst</label>
                                        <textarea class="form-control  {{ $errors->has('tekst') ? 'is-invalid' : '' }}"
                                                  id="tekst" name="tekst" style="min-height: 435px;"
                                                  placeholder="Tekst" v-model="tekst"></textarea>
                                        @if ($errors->has('tekst'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('tekst') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div v-html="compiledMarkdown"></div>
                                </div>
                            </div>
                        </markdown-preview>
                        <div class="form-group row">
                            <div class="col-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Publiceren</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection