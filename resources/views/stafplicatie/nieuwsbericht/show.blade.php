@extends('stafplicatie.layouts.app')

@section('content')
    <nieuwsbericht-editor :nieuwsbericht="{{$nieuwsbericht}}"></nieuwsbericht-editor>
@endsection