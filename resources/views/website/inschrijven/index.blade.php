@extends('website.layouts.website')

@section('content')
<inschrijf-formulier-page inline-template payment-type="{{old('type', 'ideal')}}">
    <div>
        <div class="box text-box">
            <h2 class="box-title"> Inschrijven voor Haag Uit {{ $kampjaar->jaar }}</h2>
            <p>
                Als je aankomend student van de Faculteit voor IT &amp; Design bent aan De Haagse Hogeschool kan je
                je hier inschrijven voor Haag Uit {{ $kampjaar->jaar }}.
                Velden die aangegeven zijn met een sterretje zijn verplicht. Mocht je vragen hebben, neem dan
                <a href="/contact" target="_blank">contact</a> met ons op.
            </p>
        </div>
        <div class="box text-box">
            <h2 class="box-title">Inschrijfformulier</h2>
            @if($errors->has('g-recaptcha-response'))
            <p>
                <span style="border: 1px solid red; padding: 3px 5px; border-radius: 3px;">
                    Er is iets mis gegaan bij het verifiëren van je aanvraag.
                    Probeer het formulier opnieuw te versturen.
                </span>
                <span>
                    {{ $errors->first('g-recaptcha-response') }}
                </span>
            </p>
            @endif
            <form action="/inschrijven" method="post" @submit="send">
                <p>
                    <label for="av" class="wide">
                        <input type="checkbox" name="av" id="av" class="form-control" value="1" {{ old('av') ? 'checked'
                            : '' }} required>
                        Ja, ik neem deel aan Haag Uit à €{{ $kampjaar->inschrijfbedrag }} en ga akkoord met de <a
                            href="pagina/av">Algemene Voorwaarden</a></label>
                    @if($errors->has('av'))
                    <span class="error">{{ $errors->first('av') }}</span>
                    @endif
                </p>
                <p>
                    <label for="annuleringsverzekering" class="wide">
                        <input type="checkbox" name="annuleringsverzekering" id="annuleringsverzekering"
                            class="form-control" value="1" {{ old('annuleringsverzekering') ? 'checked' : '' }}>
                        Ja, ik sluit een annuleringsverzekering af via Haag Uit à €{{$kampjaar->verzekeringbedrag}}.
                    </label>
                </p>

                <h3 class="box-sub-title">
                    Persoonsgegevens
                    <span class="subtle italic">Velden met een * zijn verplicht</span>
                </h3>
                <p>
                    <label for="roepnaam">Roepnaam*</label>
                    <input type="text" id="roepnaam" name="roepnaam" placeholder="Roepnaam" class="form-control"
                        maxlength="30" required value="{{ old('roepnaam') }}">
                    @if($errors->has('roepnaam'))
                    <span class="error">{{$errors->first('roepnaam')}}</span>
                    @endif
                </p>
                <p>
                    <label for="roepnaam">Voornaam*</label>
                    <input type="text" id="voornaam" name="voornaam" placeholder="Voornaam" class="form-control"
                        maxlength="30" required value="{{ old('voornaam') }}">
                    @if($errors->has('voornaam'))
                    <span class="error">{{$errors->first('voornaam')}}</span>
                    @endif
                </p>
                <p>
                    <label for="voorletters">Voorletters*</label>
                    <input type="text" id="voorletters" name="voorletters" placeholder="Voorletters"
                        class="form-control" maxlength="10" required value="{{ old('voorletters') }}">
                    @if($errors->has('voorletters'))
                    <span class="error">{{$errors->first('voorletters')}}</span>
                    @endif
                </p>
                <p>
                    <label for="achternaam">Achternaam*</label>
                    <input type="text" id="achternaam" name="achternaam" placeholder="Achternaam" class="form-control"
                        maxlength="30" required value="{{ old('achternaam') }}">
                    @if($errors->has('achternaam'))
                    <span class="error">{{$errors->first('achternaam')}}</span>
                    @endif
                </p>
                <p>
                    <label for="geboortedatum">Geboortedatum*</label>
                    <birthdate-picker-component old-value="{{old('geboortedatum')}}"></birthdate-picker-component>
                    @if($errors->has('geboortedatum'))
                    <span class="error">{{$errors->first('geboortedatum')}}</span>
                    @endif
                </p>
                <p>
                    <label for="geslacht">Geslacht*</label>
                    Man <input type="radio" id="geslachtMan" name="geslacht" value="m" class="form-control" {{
                        old('geslacht')==='m' ? 'checked' : '' }}>
                    Vrouw <input type="radio" id="geslachtVrouw" name="geslacht" value="v" class="form-control" {{
                        old('geslacht')==='v' ? 'checked' : '' }}>
                    Non-Binair <input type="radio" id="geslachtNonbinary" name="geslacht" value="nb"
                        class="form-control" {{ old('geslacht')==='nb' ? 'checked' : '' }}>
                </p>
                <p>
                    <label for="studie_id" class="select">Studie*</label>
                    <select name="studie_id" id="studie_id">
                        <option value="1" {{ old('studie_id') !=='1' ? '' : 'selected' }}>CMD</option>
                        <option value="2" {{ old('studie_id')==='2' ? 'selected' : '' }}>HBO-ICT</option>
                        <option value="3" {{ old('studie_id')==='3' ? 'selected' : '' }}>ADS & AI</option>
                    </select>
                    @if($errors->has('studie_id'))
                    <span class="error">{{$errors->first('studie_id')}}</span>
                    @endif
                </p>
                <p>
                    <label for="kledingmaat" class="select" tabindex="11">Voorkeur shirtmaat*</label>
                    <select name="kledingmaat" id="kledingmaat">
                        <option value="xs" {{ old('kledingmaat')==='xs' ? 'selected' : '' }}>Extra small</option>
                        <option value="s" {{ old('kledingmaat')==='s' ? 'selected' : '' }}>Small</option>
                        <option value="m" {{ old('kledingmaat')==='m' ? 'selected' : '' }}>Medium</option>
                        <option value="l" {{ old('kledingmaat')==='l' ? 'selected' : '' }}>Large</option>
                        <option value="xl" {{ old('kledingmaat')==='xl' ? 'selected' : '' }}>Extra large</option>
                    </select>
                    @if($errors->has('kledingmaat'))
                    <span class="error">{{$errors->first('kledingmaat')}}</span>
                    @endif
                </p>
                <p>
                    <label for="opmerking" tabindex="12">Opmerkingen<br />
                        <span class="subtle">
                            Denk aan medische condities, allergie informatie of gewoon een leuk verhaaltje
                        </span>
                    </label>
                    <textarea name="opmerking" id="opmerking" class="form-control" cols="40" rows="10"
                        style="resize: none" maxlength="750">{{ old('opmerking') }}</textarea>
                    @if($errors->has('opmerking'))
                    <span class="error">{{$errors->first('opmerking')}}</span>
                    @endif
                </p>
                <div class="separator"></div>

                <h3 class="box-sub-title">Contactgegevens <span class="subtle italic">Velden met een * zijn
                        verplicht</span>
                </h3>
                <p>
                    <label for="straat">Straat*</label>
                    <input type="text" id="straat" name="straat" placeholder="Spui" maxlength="50" required
                        value="{{ old('straat') }}">
                    @if($errors->has('straat'))
                    <span class="error">{{$errors->first('straat')}}</span>
                    @endif
                </p>
                <p>
                    <label for="huisnummer">Huisnummer*<span class="subtle italic"> + toevoeging</span></label>
                    <input type="text" name="huisnummer" id="huisnummer" placeholder="70a" maxlength="10" required
                        value="{{ old('huisnummer') }}">
                    @if($errors->has('huisnummer'))
                    <span class="error">{{$errors->first('huisnummer')}}</span>
                    @endif
                </p>
                <p>
                    <label for="postcode">Postcode*</label>
                    <input type="text" name="postcode" id="postcode" placeholder="1234AB" maxlength="7" required
                        value="{{ old('postcode') }}">
                    @if($errors->has('postcode'))
                    <span class="error">{{$errors->first('postcode')}}</span>
                    @endif
                </p>
                <p>
                    <label for="woonplaats">Woonplaats*</label>
                    <input type="text" name="woonplaats" id="woonplaats" placeholder="Den Haag" maxlength="75" required
                        value="{{ old('woonplaats') }}">
                    @if($errors->has('woonplaats'))
                    <span class="error">{{$errors->first('woonplaats')}}</span>
                    @endif
                </p>
                <p>
                    <label for="mobiel">Mobiele nummer*</label>
                    <input type="text" name="mobiel" id="mobiel" placeholder="0612345678" maxlength="10" required
                        value="{{ old('mobiel') }}">
                    @if($errors->has('mobiel'))
                    <span class="error">{{$errors->first('mobiel')}}</span>
                    @endif
                </p>
                <p>
                    <label for="telefoon_nood">
                        Telefoonnummer*
                        <span class="subtle italic">in geval van nood</span>
                    </label>
                    <input type="text" name="telefoon_nood" id="telefoon_nood" placeholder="0612345678" maxlength="10"
                        required value="{{ old('telefoon_nood') }}">
                    @if($errors->has('telefoon_nood'))
                    <span class="error">{{$errors->first('telefoon_nood')}}</span>
                    @endif
                </p>
                <p>
                    <label for="email">E-mailadres*</label>
                    <input type="email" name="email" id="email" placeholder="jij@gmail.com" maxlength="75" required
                        value="{{ old('email') }}">
                    @if($errors->has('email'))
                    <span class="error">{{$errors->first('email')}}</span>
                    @endif
                </p>

                <p>
                    <label></label>
                    iDeal <input type="radio" id="ideal" name="type" value="ideal" v-model="type" class="form-control">
                    Incasso <input type="radio" id="incasso" name="type" value="incasso" v-model="type"
                        class="form-control">
                </p>

                <div v-if="isIncasso">
                    <p>
                        <label>Rekeninghouder:</label>
                        <input type="text" id="rekeninghouder_naam" name="rekeninghouder_naam"
                            placeholder="A.J. de Vries" class="form-control" maxlength="20" required
                            value="{{ old('rekeninghouder_naam') }}">
                        @if($errors->has('rekeninghouder_naam'))
                        <span class="error">{{$errors->first('rekeninghouder_naam')}}</span>
                        @endif
                    </p>
                    <p>
                        <label>Rekeningnummer:</label>
                        <input type="text" id="rekeninghouder_nummer" name="rekeninghouder_nummer"
                            placeholder="NL31ABNA0123312873" class="form-control" maxlength="34" required
                            value="{{ old('rekeninghouder_nummer') }}">
                        @if($errors->has('rekeninghouder_nummer'))
                        <span class="error">{{$errors->first('rekeninghouder_nummer')}}</span>
                        @endif
                    </p>
                    <p>
                        <label>Rekeninghouder woonplaats:</label>
                        <input type="text" id="rekeninghouder_woonplaats" name="rekeninghouder_woonplaats"
                            placeholder="Den Haag" class="form-control" maxlength="20" required
                            value="{{ old('rekeninghouder_woonplaats') }}">
                        @if($errors->has('rekeninghouder_woonplaats'))
                        <span class="error">{{$errors->first('rekeninghouder_woonplaats')}}</span>
                        @endif
                    </p>
                </div>
                <div id="registratie_id"></div>
                {{ csrf_field() }}
                <label for="inschrijven"></label>
                <input type="submit" id="inschrijven" name="inschrijven" value="Inschrijving versturen"
                    :disabled="processing">
                <div v-if="processing" class="d-flex align-items-center">
                    <label></label>
                    <span class="d-flex align-items-center">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="spinner" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                            class="svg-inline--fa fa-spinner fa-w-16 fa-spin fa-lg"
                            style="height: 23px; vertical-align: middle;">
                            <path fill="currentColor"
                                d="M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"
                                class=""></path>
                        </svg>
                        <span style="vertical-align: middle">Bezig met het verwerken van je inschrijving. Even geduld
                            aub!</span>
                    </span>
                </div>
            </form>
        </div>
        {!! GoogleReCaptchaV3::render(['registratie_id' => 'registratie']) !!}
    </div>
</inschrijf-formulier-page>
@endsection