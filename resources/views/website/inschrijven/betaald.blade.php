@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Inschrijving gelukt!</h2>
        <p>
            Wat tof dat je mee gaat op Haag Uit. Jij gaat de beste start van je studie tegemoet!
            We zullen je verder op de hoogte houden via het door jou opgegeven e-mailadres, <b>{{ $email }}</b>.
        </p>
        <p>
            @if($betaling->isPaid())
                Je betaling is goed gegaan en afgerond. Dank daarvoor!
            @elseif($betaling->isOpen())
                <b>De betaling is nog niet voltooid. Klik <a href="{{ $paymentUrl }}">hier</a> om te betalen.
                Als je al betaald hebt, probeer dan de pagina te verversen. Lost het probleem zich niet op?
                Neem dan contact op met de staf!</b>
            @else
                <b>Er is iets mis gegaan. Klik <a href="{{ route('retryPayment', ['id' => $inschrijving_id]) }}">hier</a> om het opnieuw te proberen.
                    (Status: {{ $betaling->status }})</b>
            @endif
        </p>
        <p>
            Heb je nog vragen of andere dingen? Stuur dan een mailtje naar <a href="mailto:staf@haaguit.com">staf@haaguit.com</a>!
        </p>
        <p>
            We zien je op Haag Uit!
        </p>
    </div>
@endsection