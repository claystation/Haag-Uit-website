<html lang="nl">

<head>
    <title>({{ $totaal }}) LEKKER BELLEN!!</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--Courtesy of: Huis ICT'er-->

    <link href="{{ mix('assets/website/css/lekkerbellen.css') }}" rel="stylesheet">
</head>

<body>
    <div id="lekkerbellen">
        <h1 class="page-header">Hallo Haag Uit!</h1>
        <h2 class="page-header">Voor Haag Uit {{ $kampjaar->jaar }} hebben we al:</h2>

        <div class="total-inschrijvingen">{{ $totaal }} Inschrijvingen!</div>

        <div class="verhouding-box">Verhouding:</div>

        <div class="verhouding-box">
            @if($totaal >= 1)
            Staffers: Paaltjes (100%) | Niet paaltjes: (0%)
            @else
            Volgende op <b>1!</b> of meer inschrijvingen!
            @endif
        </div>
        <div class="verhouding-box">
            @if($totaal >= 10)
            Man: {{ $geslacht['man'] }} ({{ ($geslacht['man'] / $totaal) * 100 }}%) | Vrouw:
            {{ $geslacht['vrouw'] }} ({{($geslacht['vrouw'] / $totaal) * 100 }}%) | Non-binair:
            {{ $geslacht['non-binair'] }} ({{($geslacht['non-binair'] / $totaal) * 100 }}%)
            @else
            Volgende op 10 of meer inschrijvingen!
            @endif
        </div>
        <div class="verhouding-box">
            @if($totaal >= 25)
            CMD: {{ $opleiding['cmd'] }} ({{($opleiding['cmd'] / $totaal) * 100 }}%) | HBO-ICT:
            {{ $opleiding['hbo-ict'] }} ({{($opleiding['hbo-ict'] / $totaal) * 100 }}%) | ADS-AI:
            {{ $opleiding['ads-ai'] }} ({{($opleiding['ads-ai'] / $totaal) * 100 }}%)
            @else
            Volgende op 25 of meer inschrijvingen!
            @endif
        </div>
        <div class="verhouding-box">

            @if($totaal >= 40)
            Meerderjarig: {{ $leeftijd['meerderjarig'] }} ({{($leeftijd['meerderjarig'] /
            $totaal) * 100 }}%) | Minderjarig: {{ $leeftijd['minderjarig'] }}
            ({{($leeftijd['minderjarig'] / $totaal) * 100 }}%)
            @else
            Volgende op 40 of meer inschrijvingen!
            @endif
        </div>
        <div class="verhouding-box">
            @if($totaal >= 70)
            DTF: Mees (100%) | Non-DTF: 0 (0%)
            @else
            Volgende op 70 of meer inschrijvingen!
            @endif
        </div>

        <div class="verhouding-box">
            @if($totaal >= 80)
            <airhorn-component></airhorn-component>
            @else
            <b>AIRHORN OP 80!!!</b>
            @endif
        </div>
        <div class="verhouding-box">
            @if($totaal >= 100)
            <h2> Voor Haag Uit {{ $kampjaar->jaar - 1 }} hadden we in totaal:</h2>

            <div class="total-inschrijvingen">{{ $totaalVorigJaar }} Inschrijvingen</div>

            <div class="boxed">
                Het is vandaag: {{ \Carbon\Carbon::now()->format('d-m-Y') }}
                <br />
                <br />
                Vandaag op deze dag in {{ $kampjaar->jaar - 1 }}
                ({{ \Carbon\Carbon::now()->subYear()->format('d-m-Y') }}) hadden we al:
                <b>{{ $totaalVorigJaarVandaag }}</b> inschrijvingen!
            </div>
            @else
            Volgende op <b>100!</b> of meer inschrijvingen!
            @endif
        </div>
        <h1>#LEKKERBELLEN!</h1><br />
    </div>
    <!-- Scripts -->
    <script src="{{ mix('/assets/stafplicatie/js/manifest.js') }}" defer></script>
    <script src="{{ mix('/assets/stafplicatie/js/vendor.js') }}" defer></script>
    <script src="{{ mix('assets/website/js/lekkerbellen.js') }}" defer></script>
</body>

</html>