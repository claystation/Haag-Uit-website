@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">{{ $pagina->titel }}</h2>
        {!! $pagina->tekst !!}
    </div>
@endsection
