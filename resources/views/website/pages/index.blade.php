@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">{{ $pagina->titel }}</h2>
        {!! $pagina->tekst !!}
    </div>

    <latest-news-component></latest-news-component>

    <div class="box text-box">
        <h2 class="box-title">{{ $promo->titel }}</h2>
        <p>
            {!! $promo->tekst !!}
        </p>
    </div>
@endsection
