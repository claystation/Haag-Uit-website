@component('email.layout.message')

# Bericht via de website
<hr><br>

Er is een vraag via de website van {{ $name }} ({{ $email }}):

{{ $text }}

@endcomponent