@component('email.layout.message')
# Inschrijving compleet
<hr><br>

Beste {{ $inschrijving->persoon->roepnaam }},

Bij deze bevestigen we je inschrijving voor Haag Uit {{ $inschrijving->kampjaar->jaar }}. Leuk dat je mee gaat!

Op onze website staat alle informatie zoals wanneer we precies vertrekken en wat je mee moet nemen.
Via de website en e-mail zullen we je op de hoogte houden van eventuele laatste wijzigingen.

@if($inschrijving->betalingen()->latest()->first()->type == 'ideal')
Je hebt er voor gekozen om te betalen middels iDeal. Heb je nog niet betaald? Klik dan <a href="{{ $paymentUrl }}">hier</a> om te betalen.
Heb je al betaald? Dan hoef je verder niets te doen!

(Werkt de link niet? Plak dan deze URL in je browser: {{$paymentUrl}})
@else
Je hebt er voor gekozen om te betalen middels incasso. De datum voor de incasso wordt nader bekend gemaakt. Hier
zal je verder bericht over ontvangen via de mail. Het te incasseren bedrag is <b>&euro; {{ $inschrijving->betalingen()->latest()->first()->bedrag }}</b>.
Je krijgt minimaal 2 weken voor de incasso datum bericht.
@endif

Heb je nog vragen? Reageer dan op deze email of stuur een email naar <a href="mailto:staf@haaguit.com">staf@haaguit.com</a>.

Tot ziens op Haag Uit!

Met vriendelijke groet,

Staf Haag Uit
@endcomponent