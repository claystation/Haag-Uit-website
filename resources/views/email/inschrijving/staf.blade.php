@component('email.layout.message')
# Nieuwe inschrijving voor Haag Uit {{ $inschrijving->kampjaar->jaar }}
<hr><br>

Hallo Staf,

Er is een nieuwe inschrijving voor Haag Uit {{ $inschrijving->kampjaar->jaar }}!

<table style="margin: 30px auto; width: 100%;">
    <tbody style="box-sizing: border-box;">
    <tr>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">Naam</td>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">{{ $inschrijving->persoon->voornaam }} {{ $inschrijving->persoon->achternaam }}</td>
    </tr>
    <tr>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">Email</td>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">{{ $inschrijving->persoon->email }}</td>
    </tr>
    <tr>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">Geboortedatum</td>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">{{ $inschrijving->persoon->geboortedatum->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">Geslacht</td>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">{{ $inschrijving->persoon->geslacht }}</td>
    </tr>
    <tr>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">Betalingswijze</td>
        <td style="font-size: 15px; line-height: 18px; padding: 10px 0;">{{ $inschrijving->betalingen()->latest()->first()->type }}</td>
    </tr>
    </tbody>
</table>

Met vriendelijke groet,

HUplicatie
@endcomponent