/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Echo from "laravel-echo"
import AirhornComponent from './components/AirhornComponent';

window.Vue = require('vue');

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    wsHost: window.location.hostname,
    wsPort: 443,
    disableStats: true,
    encrypted: true,
    namespace: 'HUplicatie.Events'
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('airhorn-component', AirhornComponent);

const lekkerbellen = new Vue({
    el: '#lekkerbellen',

    data() {
        return {

        }
    },

    created() {
        console.log('Lekker bellen!');
    }
});
