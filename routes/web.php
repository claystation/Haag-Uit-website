<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Stafplicatie')->prefix('/stafplicatie')->group(function () {
    Auth::routes(['register' => false]);

    Route::middleware('auth')->group(function () {
        Route::redirect('/', '/stafplicatie/dashboard');
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        Route::get('/aanwezigheid', 'AanwezigheidController@index');
        Route::put('/aanwezigheid/{aanwezigheid}', 'AanwezigheidController@update');

        Route::resource('user', 'UserController');
        Route::resource('kampjaar', 'KampjaarController');
        Route::resource('inschrijving', 'InschrijvingController');
        Route::resource('nieuwsbericht', 'NieuwsberichtController')->except('edit');
        Route::resource('actiepunt', 'ActiepuntController')->except('create', 'edit');
        Route::resource('persoon', 'PersoonController')->except('create');
        Route::resource('sponsor', 'SponsorController')->except('show');
        Route::resource('pagina', 'PaginaController')->only(['index', 'show', 'edit', 'update']);

        Route::resource('medewerker', 'MedewerkerController')->except(['show']);

        Route::prefix('/api')->group(function () {
            Route::post('/kampjaar/{kampjaar}/activate', 'Api\Kampjaar\KampjaarStatusController@store');
            Route::post('/kampjaar/{kampjaar}/open', 'Api\Kampjaar\KampjaarRegistrationStatusController@store');
            Route::post('/kampjaar/{kampjaar}/close', 'Api\Kampjaar\KampjaarRegistrationStatusController@destroy');

            Route::post('/sponsor/{sponsor}/activate', 'SponsorController@restore');
            Route::delete('/sponsor/{sponsor}/activate', 'SponsorController@remove');

            Route::get('/inschrijving', 'Api\Inschrijving\InschrijvingQueryController@index');
            Route::get('/inschrijving/export', 'Api\Inschrijving\InschrijvingExportController@export');
            Route::post(
                '/inschrijving/{inschrijving}/open',
                'Api\Inschrijving\InschrijvingPresentStatusController@open'
            );
            Route::post(
                '/inschrijving/{inschrijving}/unregister',
                'Api\Inschrijving\InschrijvingPresentStatusController@unregister'
            );

            Route::post('/betaling/{betaling}/open', 'Api\Inschrijving\InschrijvingPaymentStatusController@open');
            Route::post('/betaling/{betaling}/paid', 'Api\Inschrijving\InschrijvingPaymentStatusController@paid');

            Route::get('/pagina/{pagina}', 'Api\Pagina\PaginaContentController@index');
            Route::post('/pagina/{pagina}', 'Api\Pagina\PaginaContentController@update');

            Route::get('/actiepunt/users', 'Api\Actiepunt\ActiepuntenUsersController@index');
            Route::post('/actiepunt/{actiepunt}/status', 'Api\Actiepunt\ActiepuntenStatusController@store');
            Route::delete('/actiepunt/{actiepunt}/status', 'Api\Actiepunt\ActiepuntenStatusController@destroy');

            Route::get(
                '/statistics/{kampjaar}/registrations',
                'Api\Statistics\RegistrationStatisticsController@registrationStatistics'
            );
            Route::get(
                '/statistics/{kampjaar}/actiepunten',
                'Api\Statistics\ActiepuntenStatisticsController@actiepuntenStatistics'
            );

            Route::get('/medewerker', 'Api\Medewerker\MedewerkerApiController@index');
        });

        Route::prefix('/mails')->group(function () {
            Route::get('/contact', 'Email\EmailPreviewController@contactMail');

            Route::get('/inschrijvingpersoon/ideal', 'Email\EmailPreviewController@inschrijvingPersoonIdealMail');
            Route::get('/inschrijvingpersoon/incasso', 'Email\EmailPreviewController@inschrijvingPersoonIncassoMail');

            Route::get('/inschrijvingstaf/ideal', 'Email\EmailPreviewController@inschrijvingIdealStafMail');
            Route::get('/inschrijvingstaf/incasso', 'Email\EmailPreviewController@inschrijvingIncassoStafMail');
        });
    });
});

Route::namespace('Website')->group(function () {
    Route::get('/', 'PaginaController@index');
    Route::get('/pagina/{pagina?}', 'PaginaController@show');

    Route::get('/nieuws', 'NieuwsberichtController@index');
    Route::get('/nieuws/{nieuwsbericht}', 'NieuwsberichtController@show');

    Route::get('/contact', 'ContactController@index');
    Route::post('/contact', 'ContactController@store');

    Route::get('/api/nieuws/feed', 'Api\NieuwsFeedController@index');
    Route::get('/api/sponsor/feed', 'Api\SponsorFeedController@index');

    Route::get('/inschrijven', 'RegistratieController@index');
    Route::post('/inschrijven', 'RegistratieController@store');
    Route::get('/inschrijven/gesloten', 'RegistratieController@closed');
    Route::get('/inschrijven/betaald', 'BetalingController@returnUrl')->name('returnUrl');

    Route::get('/betalingen/opnieuw', 'BetalingController@retry')->name('retryPayment');
    Route::post('/betalingen/webhook', 'BetalingController@webhook')->name('paymentsWebhook');

    Route::middleware('auth')->group(function () {
        Route::get('/lekkerbellen', 'LekkerBellenController@index');
    });
});
