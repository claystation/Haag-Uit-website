<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Betaling;
use HUplicatie\IncassoBetaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Persoon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class KampInschrijvingTest extends TestCase
{
    use RefreshDatabase;

    private $inschrijving;

    protected function setUp(): void
    {
        parent::setUp();

        $this->inschrijving = create(KampInschrijving::class);
    }

    /**
     * Test if a kamp inschrijving has a persoon.
     *
     * @test
     */
    public function a_kamp_inschrijving_has_a_persoon(): void
    {
        $this->assertInstanceOf(Persoon::class, $this->inschrijving->persoon);
    }

    /**
     * Test if a kamp inschrijving has a betaling.
     *
     * @test
     */
    public function a_kamp_inschrijving_has_a_betaling(): void
    {
        $incassoBetaling = new IncassoBetaling('123456789', '100.00');
        $this->inschrijving->addBetaling($incassoBetaling);
        $this->assertInstanceOf(Betaling::class, $this->inschrijving->betalingen()->first());
    }

    /**
     * Test if a kamp inschrijving belongs to a kampjaar.
     *
     * @test
     */
    public function a_kamp_inschrijving_belongs_to_a_kampjaar(): void
    {
        $this->assertInstanceOf(Kampjaar::class, $this->inschrijving->kampjaar);
    }

    /**
     * Test if a new incasso betaling can be added to a kamp inschrijving.
     *
     * @test
     */
    public function a_new_incasso_betaling_can_be_added_to_a_kamp_inschrijving(): void
    {
        $betaling = new IncassoBetaling('1234567890', '100.00');
        $betaling->setRekeninghouderNaam('rekeninghouder naam');
        $betaling->setRekeninghouderNummer('rekeninghouder nummer');
        $betaling->setRekeninghouderPlaats('rekeninghouder woonplaats');

        $this->inschrijving->addBetaling($betaling);

        $this->assertDatabaseHas(
            'betalingen',
            ['bedrag' => '100.00', 'type' => 'incasso', 'kamp_inschrijving_id' => $this->inschrijving->id]
        );
    }
}
