<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Actiepunt;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a user can have multiple actiepunten.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_have_multiple_actiepunten(): void
    {
        $user = create(User::class);
        $kampjaar = app(KampjaarFactory::class)->isActief()->create(2019);
        $actiepunten = create(Actiepunt::class, ['jaar' => $kampjaar->jaar], 5);

        $user->actiepunten()->attach($actiepunten);

        $this->assertCount(5, $user->fresh()->actiepunten);
    }
}
