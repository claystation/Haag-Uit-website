<?php

namespace Tests\Stafplicatie\Unit;

use function create;
use HUplicatie\Aanwezigheid;
use HUplicatie\Functie;
use HUplicatie\Medewerker;
use HUplicatie\Persoon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AanwezigheidTest extends TestCase
{
    use RefreshDatabase;

    private $aanwezigheid;

    protected function setUp(): void
    {
        parent::setUp();

        $this->aanwezigheid = create(Aanwezigheid::class);
    }

    /**
     * Test aanwezigheid belonging to a medewerker.
     *
     * @test
     */
    public function aanwezigheid_belongs_to_a_medewerker(): void
    {
        $this->assertInstanceOf(Medewerker::class, $this->aanwezigheid->medewerker);
    }
}
