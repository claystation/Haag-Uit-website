<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SponsorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a sponsor can determine its logo path.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_determine_its_logo_path(): void
    {
        $sponsor = create(Sponsor::class, ['logo' => 'sponsoren/svsim.png']);
        $this->assertEquals(asset('/storage/sponsoren/svsim.png'), $sponsor->logo);
    }
}
