<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ActivateKampjaarTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if an kampjaar can be activated.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_activated(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 0]);
        $this->withExceptionHandling();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/activate")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/activate")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/activate")
            ->assertStatus(200);

        $kampjaar->refresh();
        $this->assertTrue($kampjaar->actief);
        $this->assertEquals(Kampjaar::getActiveKampjaar(), $kampjaar->fresh());
    }

    /**
     * Test if an active kampjaar gets deactivated once another kampjaar is activated.
     *
     * @test
     *
     * @return void
     */
    public function when_one_kampjaar_is_activated_the_other_gets_deactivated(): void
    {
        $oldKampjaar = create(Kampjaar::class, ['jaar' => 2000, 'actief' => 1]);
        $newKampjaar = create(Kampjaar::class, ['jaar' => 2001, 'actief' => 0]);

        $this->withExceptionHandling();
        $this->signInWithPermissions('Edit Kampjaar');
        $this->post("/stafplicatie/api/kampjaar/{$newKampjaar->jaar}/activate")
            ->assertStatus(200);

        $newKampjaar->refresh();
        $this->assertTrue($newKampjaar->actief);
        $this->assertEquals(Kampjaar::getActiveKampjaar(), $newKampjaar->fresh());
        $this->assertFalse($oldKampjaar->fresh()->actief);
    }
}
