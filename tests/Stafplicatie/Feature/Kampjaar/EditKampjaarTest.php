<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditKampjaarTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('public');
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a kampjaar can be edited.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_edited(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2018]);

        $this->get('/stafplicatie/kampjaar/2018/edit')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/kampjaar/2018/edit')
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->get('/stafplicatie/kampjaar/2018/edit')
            ->assertSee($kampjaar->jaar)
            ->assertSee($kampjaar->logo);
    }

    /**
     * Test if a kampjaar can be updated without a new logo.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_updated_without_a_new_logo(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2018]);

        $this->put('/stafplicatie/kampjaar/2018')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put('/stafplicatie/kampjaar/2018')
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->withExceptionHandling()->json('PUT', '/stafplicatie/kampjaar/2018', [
            'start'             => '24-08-2019',
            'eind'              => '28-08-2019',
            'inschrijfbedrag'   => 150,
            'verzekeringbedrag' => 15.00,
        ])
            ->assertSessionDoesntHaveErrors()
            ->assertRedirect('/stafplicatie/kampjaar')
            ->assertSessionHas('flash', 'Kampjaar gewijzigd');

        $kampjaar->refresh();
        $this->assertEquals('24-08-2019', $kampjaar->start);
        $this->assertEquals('28-08-2019', $kampjaar->eind);
        $this->assertEquals(150.00, $kampjaar->inschrijfbedrag);
        $this->assertEquals(15.00, $kampjaar->verzekeringbedrag);
        $this->assertEquals(asset('/assets/website/img/hu_logo_default.png'), $kampjaar->logo);
    }

    /**
     * Test if a kampjaar can be updated with a new logo.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_updated_with_a_new_logo(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2018]);

        $this->put('/stafplicatie/kampjaar/2018')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put('/stafplicatie/kampjaar/2018')
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->withExceptionHandling()->json('PUT', '/stafplicatie/kampjaar/2018', [
            'start'             => '24-08-2019',
            'eind'              => '28-08-2019',
            'inschrijfbedrag'   => 150,
            'verzekeringbedrag' => 15.00,
            'logo'              => $logo = UploadedFile::fake()->image('albertheijn.png'),
        ])
            ->assertSessionDoesntHaveErrors()
            ->assertRedirect('/stafplicatie/kampjaar')
            ->assertSessionHas('flash', 'Kampjaar gewijzigd');

        $kampjaar->refresh();
        $this->assertEquals('24-08-2019', $kampjaar->start);
        $this->assertEquals('28-08-2019', $kampjaar->eind);
        $this->assertEquals(150.00, $kampjaar->inschrijfbedrag);
        $this->assertEquals(15.00, $kampjaar->verzekeringbedrag);
        $this->assertEquals(asset('storage/logos/'.$logo->hashName()), $kampjaar->logo);
    }

    /**
     * Test if kampjaar updates are validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        create(Kampjaar::class, ['jaar' => 2018]);

        $this->signInWithPermissions('Edit Kampjaar');
        $this->put('/stafplicatie/kampjaar/2018', [])
            ->assertStatus(302)
            ->assertSessionHasErrors(['start', 'eind', 'inschrijfbedrag', 'verzekeringbedrag']);
    }
}
