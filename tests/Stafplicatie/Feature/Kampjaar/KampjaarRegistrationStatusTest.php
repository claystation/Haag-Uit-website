<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class KampjaarRegistrationStatusTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a kampjaar can be opened.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_opened(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 0]);
        $this->withExceptionHandling();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/open")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/open")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/open")
            ->assertStatus(200);

        $kampjaar->refresh();
        $this->assertTrue($kampjaar->actief);
        $this->assertTrue($kampjaar->open);
        $this->assertEquals(Kampjaar::getOpenKampjaar(), $kampjaar->fresh());
    }

    /**
     * Test if a kampjaar can be closed.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_closed(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);
        $this->withExceptionHandling();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/close")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/close")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Kampjaar');
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/close")
            ->assertStatus(200);

        $kampjaar->refresh();
        $this->assertTrue($kampjaar->actief);
        $this->assertFalse($kampjaar->open);
        $this->assertNull(Kampjaar::getOpenKampjaar());
    }

    /**
     * Test if a kampjaar cannot be opened when not active.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_not_be_opened_when_not_active(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 0, 'open' => 0]);
        $this->signInWithPermissions('Edit Kampjaar');
        $this->post("/stafplicatie/api/kampjaar/{$kampjaar->jaar}/open")
            ->assertStatus(409);

        $kampjaar->refresh();
        $this->assertFalse($kampjaar->actief);
        $this->assertFalse($kampjaar->open);
    }
}
