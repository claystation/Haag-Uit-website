<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadKampjaarTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all kampjaren can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_kampjaren_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/kampjaar')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/kampjaar')
            ->assertSee('403');

        $this->signInWithPermissions('View Kampjaren');
        $kampjaar = create(Kampjaar::class, ['jaar' => 2019, 'actief' => 1, 'open' => 1]);
        $kampjaar2 = create(Kampjaar::class, ['jaar' => 2020, 'actief' => 0, 'open' => 0]);

        $response = $this->get('/stafplicatie/kampjaar');

        $response->assertSee($kampjaar->jaar)
            ->assertSee($kampjaar->start)
            ->assertSee($kampjaar->eind)
            ->assertSee($kampjaar->logo);

        $response->assertSee($kampjaar2->jaar)
            ->assertSee($kampjaar2->start)
            ->assertSee($kampjaar2->eind)
            ->assertSee($kampjaar2->logo);
    }

    /**
     * Test if a Kampjaar can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2019, 'actief' => 1, 'open' => 1]);
        $kampjaar2 = create(Kampjaar::class, ['jaar' => 2020, 'actief' => 0, 'open' => 0]);

        $this->get("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertSee('403');

        $this->signInWithPermissions('View Kampjaar');
        $this->get("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertSee($kampjaar->start)
            ->assertSee($kampjaar->eind)
            ->assertSee('Actief')
            ->assertSee('Open')
            ->assertSee($kampjaar->logo);

        $this->get("/stafplicatie/kampjaar/{$kampjaar2->jaar}")
            ->assertSee($kampjaar2->start)
            ->assertSee($kampjaar2->eind)
            ->assertSee('Inactief')
            ->assertSee('Gesloten')
            ->assertSee($kampjaar2->logo);
    }
}
