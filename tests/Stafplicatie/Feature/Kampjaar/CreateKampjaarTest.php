<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class CreateKampjaarTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('public');
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a form can be shown to create a kampjaar.
     *
     * @test
     *
     * @return void
     */
    public function a_create_kampjaar_form_can_be_shown(): void
    {
        $this->withExceptionHandling();

        $this->get('/stafplicatie/kampjaar/create')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/kampjaar/create')
            ->assertSee('403');

        $this->signInWithPermissions('Create Kampjaar');
        $this->get('/stafplicatie/kampjaar/create')
            ->assertOk()
            ->assertSee('Kampjaar aanmaken');
    }

    /**
     * Test if a kampjaar can be created.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_created(): void
    {
        $this->withoutExceptionHandling();
        $this->assertEquals(0, Kampjaar::count());
        $this->signInWithPermissions('Create Kampjaar');
        $response = $this->post('/stafplicatie/kampjaar', [
            'jaar'              => 2020,
            'start'             => '2020-08-20',
            'eind'              => '2020-08-26',
            'inschrijfbedrag'   => '100',
            'verzekeringbedrag' => '5.00',
            'logo'              => $logo = UploadedFile::fake()->image('hu_logo_2020.png'),
        ]);

        $response->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/kampjaar')
            ->assertSessionHas('flash', 'Kampjaar aangemaakt');

        $this->assertDatabaseHas('Kampjaren', [
            'jaar'              => 2020,
            'start'             => '2020-08-20',
            'eind'              => '2020-08-26',
            'inschrijfbedrag'   => '100.00',
            'verzekeringbedrag' => '5.00',
            'logo'              => 'logos/'.$logo->hashName(),
        ]);

        $this->assertEquals(asset('storage/logos/'.$logo->hashName()), Kampjaar::find(2020)->logo);

        Storage::disk('public')->assertExists('logos/'.$logo->hashName());
        $this->assertEquals(1, Kampjaar::count());
    }

    /**
     * Test if a kampjaar can be created without a logo.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_created_without_a_logo(): void
    {
        $this->withoutExceptionHandling();
        $this->assertEquals(0, Kampjaar::count());
        $this->signInWithPermissions('Create Kampjaar');
        $response = $this->post('/stafplicatie/kampjaar', [
            'jaar'              => 2020,
            'start'             => '2020-08-20',
            'eind'              => '2020-08-26',
            'inschrijfbedrag'   => '100',
            'verzekeringbedrag' => '5.00',
        ]);

        $response->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/kampjaar')
            ->assertSessionHas('flash', 'Kampjaar aangemaakt');

        $this->assertDatabaseHas('Kampjaren', [
            'jaar'              => 2020,
            'start'             => '2020-08-20',
            'eind'              => '2020-08-26',
            'inschrijfbedrag'   => '100.00',
            'verzekeringbedrag' => '5.00',
            'logo'              => '',
        ]);

        $this->assertEquals(asset('/assets/website/img/hu_logo_default.png'), Kampjaar::find(2020)->logo);

        $this->assertEquals(1, Kampjaar::count());
    }

    /**
     * Test if kampjaar creations are validated.
     *
     * @test
     *
     * @return void
     */
    public function kampjaar_creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Kampjaar::count());
        $this->signInWithPermissions('Create Kampjaar');
        $this->post('/stafplicatie/kampjaar', [])
            ->assertSessionHasErrors(['jaar', 'start', 'eind', 'inschrijfbedrag', 'verzekeringbedrag'])
            ->assertSessionDoesntHaveErrors(['logo']);

        $this->assertEquals(0, Kampjaar::count());
    }
}
