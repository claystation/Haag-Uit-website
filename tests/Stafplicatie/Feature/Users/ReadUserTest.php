<?php

namespace Tests\Stafplicatie\Feature\Users;

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadUserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all Users can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_users_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/user')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/user')
            ->assertSee('403');

        $this->signInWithPermissions('View Users');
        $user = create(User::class);
        $user->assignRole(Roles::STAFFER);

        $this->withoutExceptionHandling()->get('/stafplicatie/user')
            ->assertOk()
            ->assertSee(e($user->name))
            ->assertSee($user->email)
            ->assertSee(Roles::STAFFER);
    }

    /**
     * Test if a User can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class);
        $user->assignRole(Roles::STAFFER);

        $this->get("/stafplicatie/user/{$user->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/user/{$user->id}")
            ->assertSee('403');

        $this->signInWithPermissions('View User');
        $this->get("/stafplicatie/user/{$user->id}")
            ->assertOk()
            ->assertSee(e($user->name))
            ->assertSee($user->email)
            ->assertSee(Roles::STAFFER);
    }
}
