<?php

namespace Tests\Stafplicatie\Feature\Users;

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a user can be deleted.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_be_deleted(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class, ['id' => 2]);

        $this->delete("/stafplicatie/user/{$user->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/user/{$user->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Delete User');
        $this->json('DELETE', "/stafplicatie/user/{$user->id}")
            ->assertStatus(204);

        $user = create(User::class, ['id' => 2]);
        $this->delete("/stafplicatie/user/{$user->id}")
            ->assertRedirect('/stafplicatie/user');
    }

    /**
     * Test if user with id 1 cannot be deleted.
     *
     * @test
     *
     * @return void
     */
    public function user_id_1_can_not_be_deleted(): void
    {
        $this->withExceptionHandling();

        $this->signInWithPermissions('Delete User');
        $this->json('DELETE', '/stafplicatie/user/1')
            ->assertStatus(403);

        $this->signInAsRole(Roles::WEBMEESTER);
        $this->delete('/stafplicatie/user/1')
            ->assertSessionHas('flash', 'Nee sorry...');
    }

    /**
     * Test if a user cannot delete itself.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_not_delete_itself(): void
    {
        $this->withExceptionHandling();

        $this->signInWithPermissions('Delete User');
        $id = auth()->id();
        $this->json('DELETE', "/stafplicatie/user/{$id}")
            ->assertStatus(403);
    }
}
