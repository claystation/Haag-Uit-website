<?php

namespace Tests\Stafplicatie\Feature\Aanwezigheid;

use HUplicatie\Aanwezigheid;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditAanwezigheidTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permissions_are_enforced(): void
    {
        $aanwezigheid = create(Aanwezigheid::class);

        $this->withExceptionHandling();
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}")
            ->assertStatus(401);

        $this->signIn();
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}")
            ->assertStatus(403);

        $this->signInWithPermissions('Edit Medewerker');
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}", [])
            ->assertStatus(422);
    }

    /**
     * Test if aanwezigheid updates are validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        $aanwezigheid = create(Aanwezigheid::class);

        $this->signInWithPermissions('Edit Medewerker');
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}", [])
            ->assertStatus(422);
    }

    /**
     * Test if aanwezigheid can be updated.
     *
     * @test
     *
     * @return void
     */
    public function aanwezigheid_can_be_updated(): void
    {
        $this->withExceptionHandling();
        $aanwezigheid = create(Aanwezigheid::class, ['status' => 0]);

        $this->assertDatabaseHas('aanwezigheden', ['id' => $aanwezigheid->id, 'status' => 0]);

        $this->signInWithPermissions('Edit Medewerker');
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}", [
            'status' => 2,
        ])->assertStatus(200);

        $this->assertDatabaseHas('aanwezigheden', ['id' => $aanwezigheid->id, 'status' => 2]);

        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}", [
            'meeting'  => 1,
        ])->assertStatus(422);
    }

    /**
     * Test if aanwezigheid can be updated to null.
     *
     * @test
     *
     * @return void
     */
    public function aanwezigheid_can_be_updated_to_null(): void
    {
        $this->withExceptionHandling();
        $aanwezigheid = create(Aanwezigheid::class, ['status' => 1]);

        $this->assertDatabaseHas('aanwezigheden', ['id' => $aanwezigheid->id, 'status' => 1]);

        $this->signInWithPermissions('Edit Medewerker');
        $this->putJson("/stafplicatie/aanwezigheid/{$aanwezigheid->id}", [
            'status' => null,
        ])->assertStatus(200);

        $this->assertDatabaseHas('aanwezigheden', ['id' => $aanwezigheid->id, 'status' => null]);
    }
}
