<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use HUplicatie\Betaling;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class InschrijvingPaymentStatusControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if an unauthenticated user can not change payment status.
     *
     * @test
     *
     * @return void
     */
    public function an_unauthenticated_user_can_not_change_payment_status(): void
    {
        $betaling = create(Betaling::class);
        $this->withExceptionHandling();
        $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/open")
            ->assertStatus(401);

        $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/paid")
            ->assertStatus(401);
    }

    /**
     * Test if a user cannot change payment status without the right permissions.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_not_change_payment_status_without_the_right_permission(): void
    {
        $betaling = create(Betaling::class);
        $this->withExceptionHandling();
        $this->signIn();
        $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/open")
            ->assertStatus(403);
        $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/paid")
            ->assertStatus(403);
    }

    /**
     * Test if it is only possible to change the status of incasso payments.
     *
     * @test
     *
     * @return void
     */
    public function it_is_only_possible_to_change_status_of_incasso_payments(): void
    {
        $idealBetaling = create(Betaling::class, ['type' => 'ideal', 'kamp_inschrijving_id' => 1]);
        $incassoBetaling = create(Betaling::class, ['type' => 'incasso', 'kamp_inschrijving_id' => 1]);
        $this->signInWithPermissions('Edit Inschrijving');

        $response = $this->postJson("/stafplicatie/api/betaling/{$idealBetaling->id}/paid");
        $response->assertStatus(400);
        $jsonError = $response->json();
        $this->assertEquals('Dit is geen incasso betaling', $jsonError);

        $response = $this->postJson("/stafplicatie/api/betaling/{$incassoBetaling->id}/paid");
        $response->assertStatus(200);
    }

    /**
     * Test if a betaling can be marked as paid.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_marked_as_paid(): void
    {
        $betaling = create(Betaling::class, ['type' => 'incasso', 'status' => 'open']);
        $this->signInWithPermissions('Edit Inschrijving');

        $response = $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/paid");
        $response->assertStatus(200);

        $this->assertEquals('paid', $betaling->fresh()->status);
    }

    /**
     * Test if a betaling can be marked as open.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_marked_as_open(): void
    {
        $betaling = create(Betaling::class, ['type' => 'incasso', 'status' => 'paid']);
        $this->signInWithPermissions('Edit Inschrijving');

        $response = $this->postJson("/stafplicatie/api/betaling/{$betaling->id}/open");
        $response->assertStatus(200);

        $this->assertEquals('open', $betaling->fresh()->status);
    }
}
