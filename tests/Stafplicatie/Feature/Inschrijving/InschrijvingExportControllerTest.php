<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use Carbon\Carbon;
use Exception;
use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class InschrijvingExportControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all inschrijvingen can be exported.
     *
     * @test
     *
     * @return void
     *
     * @throws Exception
     */
    public function all_inschrijvingen_can_be_exported(): void
    {
        $date = Carbon::now()->format('Y-m-d');
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $inschrijvingen = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar], 5);
        $inschrijvingen->each(function ($inschrijving) {
            create(Betaling::class, ['kamp_inschrijving_id' => $inschrijving->id]);
        });

        $this->signInWithPermissions('View Inschrijvingen');

        $response = $this->get('/stafplicatie/api/inschrijving/export');

        $response->assertStatus(200);
        $response->assertHeader(
            'content-disposition',
            "attachment; filename=Inschrijvingen-Haag-Uit-{$kampjaar->jaar}-{$date}.xlsx"
        );
    }
}
