<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use Carbon\Carbon;
use HUplicatie\Betaling;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class InschrijvingQueryControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if an unauthenticated user cannot query inschrijvingen.
     *
     * @test
     *
     * @return void
     */
    public function an_unauthenticated_user_cannot_query_inschrijvingen(): void
    {
        $this->withExceptionHandling();
        $this->getJson('/stafplicatie/api/inschrijving')
            ->assertStatus(401);
    }

    /**
     * Test if an user cannot query inschrijvingen without the right permissions.
     *
     * @test
     *
     * @return void
     */
    public function an_user_cannot_query_inschrijvingen_without_the_right_permission(): void
    {
        $this->withExceptionHandling();
        $this->signIn();
        $this->getJson('/stafplicatie/api/inschrijving')
            ->assertStatus(403);
    }

    /**
     * Test if endpoint only returns inschrijvingen for the active kampjaar.
     *
     * @test
     *
     * @return void
     */
    public function api_only_returns_inschrijvingen_for_the_active_kampjaar(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(1)->isActief()->create(2019);
        $kampjaarInactief = app(KampjaarFactory::class)->withInschrijvingen(1)->create(2018);

        $response = $this->getJson('/stafplicatie/api/inschrijving');
        $response->assertStatus(200);
        $json = $response->json();

        $this->assertCount(1, $json);
        $this->assertContains(
            $kampjaar->kampInschrijvingen()->with('persoon', 'latestBetaling')->first()->toArray(),
            $json
        );
        $this->assertNotContains(
            $kampjaarInactief->kampInschrijvingen()->with('persoon', 'latestBetaling')->first()->toArray(),
            $json
        );
    }

    /**
     * Test if a user can query for inschrijvingen.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_query_for_inschrijvingen(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        app(KampjaarFactory::class)->withInschrijvingen(40)->isActief()->create(2019);

        $response = $this->getJson('/stafplicatie/api/inschrijving')->json();
        $this->assertCount(40, $response);
    }

    /**
     * Test if a user can query for inschrijvingen by payment status.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_query_for_inschrijvingen_by_payment_status(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(5)->isActief()->create(2019);
        $kampjaar->kampInschrijvingen()->first()->betalingen()->first()->update(['status' => 'paid']);

        $response = $this->getJson('/stafplicatie/api/inschrijving?paymentStatus=open')->json();
        $this->assertCount(4, $response);

        $response = $this->getJson('/stafplicatie/api/inschrijving?paymentStatus=paid')->json();
        $this->assertCount(1, $response);
    }

    /**
     * Test if a user can query inschrijvingen by payment type.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_query_for_inschrijvingen_by_payment_type(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(5)->isActief()->create(2019);

        $inschrijvingen = $kampjaar->kampInschrijvingen;

        $response = $this->getJson('/stafplicatie/api/inschrijving?paymentType=ideal')->json();
        $this->assertCount($inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->betalingen()->latest()->first()->type === 'ideal';
        })->count(), $response);

        $response = $this->getJson('/stafplicatie/api/inschrijving?paymentType=incasso')->json();
        $this->assertCount($inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->betalingen()->latest()->first()->type === 'incasso';
        })->count(), $response);
    }

    /**
     * Test if a user can query inschrijvingen by presentie.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_query_for_inschrijvingen_by_presentie(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(5)->isActief()->create(2019);

        $inschrijvingen = $kampjaar->kampInschrijvingen;
        $inschrijvingen->get(0)->update(['presentie' => 'afgemeld']);

        $response = $this->getJson('/stafplicatie/api/inschrijving?presentie=open')->json();
        $this->assertCount(4, $response);

        $response = $this->getJson('/stafplicatie/api/inschrijving?presentie=afgemeld')->json();
        $this->assertCount(1, $response);
    }

    /**
     * Test if a user is returned including the latest betaling.
     *
     * @test
     *
     * @return void
     */
    public function the_latest_betaling_is_returned_with_the_user(): void
    {
        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(1)->isActief()->create(2019);

        $inschrijving = $kampjaar->kampInschrijvingen()->first();
        $betaling = create(
            Betaling::class,
            ['kamp_inschrijving_id' => $inschrijving->id, 'created_at' => Carbon::now()->addHour()]
        );

        $this->assertCount(2, $inschrijving->betalingen);

        $response = $this->getJson('/stafplicatie/api/inschrijving')->json();

        $this->assertEquals($inschrijving->id, $response[0]['id']);
        $this->assertEquals($betaling->id, $response[0]['latest_betaling']['id']);
    }
}
