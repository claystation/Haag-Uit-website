<?php

namespace Tests\Stafplicatie\Feature\Actiepunt;

use Carbon\Carbon;
use HUplicatie\Actiepunt;
use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditActiepuntTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permissions_are_enforced(): void
    {
        $ap = create(Actiepunt::class);

        $this->withExceptionHandling();
        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertStatus(401);

        $this->signIn();
        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertStatus(403);

        $this->signInWithPermissions('Edit Actiepunt');
        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}", [])
            ->assertStatus(422);
    }

    /**
     * Test if updates are validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        $ap = create(Actiepunt::class);

        $this->signInWithPermissions('Edit Actiepunt');
        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}", [])
            ->assertStatus(422);
    }

    /**
     * Test if users are synced with new associations.
     *
     * @test
     *
     * @return void
     */
    public function users_are_synced_with_new_input(): void
    {
        $this->withExceptionHandling();
        $ap = create(Actiepunt::class);
        $users = create(User::class, [], 2);
        $users->each->assignRole(Roles::STAFFER);

        $ap->users()->sync($users);

        $this->assertCount(2, $ap->users);
        $newUser = create(User::class);
        $newUser->assignRole(Roles::STAFFER);

        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users[0]->id,
        ]);

        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users[1]->id,
        ]);

        $this->assertDatabaseMissing('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $newUser->id,
        ]);

        $beschrijving = 'Nieuwe beschrijving';
        $deadline = Carbon::now()->format('Y-m-d');

        $this->signInWithPermissions('Edit Actiepunt');
        $response = $this->withoutExceptionHandling()->patchJson("/stafplicatie/actiepunt/{$ap->id}", [
            'beschrijving'  => $beschrijving,
            'deadline'      => $deadline,
            'assignedUsers' => [
                $users[1]->only(['id', 'name']),
                $newUser->only(['id', 'name']),
            ],
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'id'            => $ap->id,
            'beschrijving'  => $beschrijving,
            'deadline'      => $deadline,
            'assignedUsers' => [$users[1]->only(['id', 'name']), $newUser->only(['id', 'name'])],
        ]);

        $this->assertDatabaseMissing('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users[0]->id,
        ]);
        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users[1]->id,
        ]);
        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $newUser->id,
        ]);
    }

    /**
     * Test if users are required.
     *
     * @test
     *
     * @return void
     */
    public function users_are_required(): void
    {
        $this->withExceptionHandling();
        $ap = create(Actiepunt::class);

        $this->signInWithPermissions('Edit Actiepunt');
        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}", [
            'beschrijving' => 'iets',
            'deadline'     => '2019-06-07',
        ])->assertStatus(422);

        $this->patchJson("/stafplicatie/actiepunt/{$ap->id}", [
            'beschrijving'  => 'iets',
            'deadline'      => '2019-06-07',
            'assignedUsers' => [],
        ])->assertStatus(422);
    }

    /**
     * Test that deadline is optional.
     *
     * @test
     *
     * @return void
     */
    public function deadline_is_optional(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class);
        $user->assignRole(Roles::STAFFER);
        $ap = create(Actiepunt::class);

        $this->signInWithPermissions('Edit Actiepunt');
        $response = $this->patchJson("/stafplicatie/actiepunt/{$ap->id}", [
            'beschrijving'  => 'iets2',
            'assignedUsers' => [$user->only('id', 'name')],
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('actiepunten', [
            'id' => $ap->id,
            'beschrijving' => 'iets2',
        ]);
    }
}
