<?php

namespace Tests\Stafplicatie\Feature\Medewerker;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ReadMedewerkersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all Medewerkers can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function the_medewerker_inschrijvingen_index_page_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/medewerker')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/medewerker')
            ->assertSee('403');

        $this->signInWithPermissions('View Medewerkers');
        $response = $this->get('/stafplicatie/medewerker');

        $response->assertOk();
    }

    /**
     * Test if all Medewerkers get returned from the API.
     *
     * @test
     */
    public function all_medewerker_inschrijvingen_can_be_retrieved_for_current_year(): void
    {
        $kampjaar = app(KampjaarFactory::class)->withMedewerkers(40)->isActief()->create(2019);

        $this->withExceptionHandling();
        $this->getJson('/stafplicatie/api/medewerker')
            ->assertStatus(401);

        $this->signIn();
        $this->getJson('/stafplicatie/api/medewerker')
            ->assertStatus(403);

        $this->signInWithPermissions('View Medewerkers');

        $response = $this->getJson('/stafplicatie/api/medewerker');
        $response->assertStatus(200)
            ->assertJsonCount(40)
            ->assertJsonFragment($kampjaar->medewerkers()->first()->toArray());
    }
}
