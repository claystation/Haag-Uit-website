<?php

namespace Tests\Stafplicatie\Feature\Nieuwsbericht;

use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditNieuwsberichtTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if nieuwsbericht updates are validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        $nieuwsbericht = create(Nieuwsbericht::class);

        $this->signInWithPermissions('Edit Nieuwsbericht');
        $this->put("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}", [])
            ->assertStatus(302)
            ->assertSessionHasErrors(['titel', 'tekst']);
    }
}
