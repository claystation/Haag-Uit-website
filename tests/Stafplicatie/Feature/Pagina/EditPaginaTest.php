<?php

namespace Tests\Stafplicatie\Feature\Pagina;

use HUplicatie\Pagina;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditPaginaTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a pagina can be edited.
     *
     * @test
     *
     * @return void
     */
    public function a_pagina_can_be_edited(): void
    {
        $pagina = Pagina::where('naam', 'paklijst')->first();

        $this->withExceptionHandling();
        $this->get("/stafplicatie/pagina/{$pagina->id}/edit")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/pagina/{$pagina->id}/edit")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Pagina');
        $response = $this->get("/stafplicatie/pagina/{$pagina->id}/edit");
        $response->assertSee('Pagina wijzigen')
            ->assertSee($pagina->id);
    }

    /**
     * Test if a page can be requested from the API.
     *
     * @test
     *
     * @return void
     */
    public function a_page_can_be_requested_from_the_api(): void
    {
        $pagina = create(Pagina::class, ['naam' => 'testpagina', 'tekst' => 'Welkom bij $JAAR$']);

        $this->withExceptionHandling();
        $this->getJson("/stafplicatie/api/pagina/{$pagina->id}")
            ->assertStatus(401);

        $this->signIn();
        $this->get("/stafplicatie/api/pagina/{$pagina->id}")
            ->assertStatus(403);

        $this->signInWithPermissions('Edit Pagina');
        $response = $this->get("/stafplicatie/api/pagina/{$pagina->id}")->json();

        $this->assertEquals($pagina->titel, $response['titel']);
        $this->assertEquals($pagina->tekst, $response['tekst']);
        $this->assertStringContainsString('$JAAR$', $response['tekst']);
    }
}
