<?php

namespace Tests\Setup;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Medewerker;

class KampjaarFactory
{
    private $actief = 0;
    private $open = 0;
    private $inschrijvingen = 0;
    private $medewerkers = 0;

    public function isActief(): KampjaarFactory
    {
        $this->actief = 1;

        return $this;
    }

    public function isOpen(): KampjaarFactory
    {
        $this->open = 1;

        return $this;
    }

    public function withInschrijvingen($count): KampjaarFactory
    {
        $this->inschrijvingen = $count;

        return $this;
    }

    public function withMedewerkers($count): KampjaarFactory
    {
        $this->medewerkers = $count;

        return $this;
    }

    public function create(int $jaar)
    {
        $kampjaar = factory(Kampjaar::class)->create([
            'jaar'   => $jaar,
            'actief' => $this->actief,
            'open'   => $this->open,
        ]);

        factory(KampInschrijving::class, $this->inschrijvingen)->create([
            'jaar' => $kampjaar->jaar,
        ])->each(function ($inschrijving) {
            factory(Betaling::class)->create(['kamp_inschrijving_id' => $inschrijving->id]);
        });

        factory(Medewerker::class, $this->medewerkers)->create([
            'jaar' => $kampjaar->jaar,
        ]);

        return $kampjaar;
    }
}
