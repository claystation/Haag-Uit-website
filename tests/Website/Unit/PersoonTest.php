<?php

namespace Tests\Website\Unit;

use Carbon\Carbon;
use HUplicatie\KampInschrijving;
use HUplicatie\Medewerker;
use HUplicatie\Persoon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PersoonTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if geboortedatum can be a string for creation.
     *
     * @test
     *
     * @return void
     */
    public function geboortedatum_can_be_a_string_for_creation(): void
    {
        $persoon = Persoon::create([
            'roepnaam'      => 'test',
            'voornaam'      => 'test',
            'voorletters'   => 'test',
            'achternaam'    => 'test',
            'geboortedatum' => '01-01-2000',
            'geslacht'      => 'm',
            'studie_id'     => '1',
            'straat'        => 'test',
            'huisnummer'    => 'test',
            'postcode'      => 'test',
            'woonplaats'    => 'test',
            'email'         => 'test',
            'mobiel'        => 'test',
        ]);

        $this->assertDatabaseHas('personen', ['id' => $persoon->id, 'geboortedatum' => '2000-01-01 00:00:00']);
    }

    /**
     * Test if a persoon has a geboortedatum.
     *
     * @test
     */
    public function a_persoon_has_a_geboortedatum(): void
    {
        $persoon = create(Persoon::class);

        $this->assertMatchesRegularExpression(
            '/\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/',
            $persoon->geboortedatum
        );
        $this->assertInstanceOf(Carbon::class, $persoon->geboortedatum);
    }

    /**
     * Test persoon years and type inschrijving.
     *
     * @test
     */
    public function a_persoon_has_an_attribute_with_years_and_registrations(): void
    {
        $persoon = create(Persoon::class);
        create(KampInschrijving::class, ['persoon_id' => $persoon->id, 'jaar' => 2011]);
        create(Medewerker::class, ['persoon_id' => $persoon->id, 'jaar' => 2012]);
        create(Medewerker::class, ['persoon_id' => $persoon->id, 'jaar' => 2013]);

        $persoon->fresh();
        $registrations = $persoon->registrations()->toArray();

        $this->assertArrayHasKey(2011, $registrations);
        $this->assertArrayHasKey(2012, $registrations);
        $this->assertArrayHasKey(2013, $registrations);
        $this->assertEquals('Feut', $registrations[2011]);
    }
}
