<?php

namespace Tests\Website\Unit;

use HUplicatie\Nieuwsbericht;
use function strlen;
use Tests\TestCase;

class NieuwsberichtTest extends TestCase
{
    /**
     * Test if a nieuwsbericht text can be truncated.
     *
     * @test
     *
     * @return void
     */
    public function a_news_message_text_can_be_truncated(): void
    {
        $nieuwsbericht = make(Nieuwsbericht::class);

        $previewText = $nieuwsbericht->previewText();

        $this->assertGreaterThanOrEqual(102, strlen($previewText));
        $this->assertStringEndsWith('...', $previewText);
    }
}
