<?php

namespace Tests\Website\Unit;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BetalingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a betaling belongs to a KampInschrijving.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_belongs_to_a_kamp_inschrijving(): void
    {
        $betaling = create(Betaling::class);
        $this->assertInstanceOf(KampInschrijving::class, $betaling->kampInschrijving);
    }

    /**
     * Test if a betaling can be incasso.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_incasso(): void
    {
        $betaling = create(Betaling::class, ['type' => 'incasso']);
        $this->assertTrue($betaling->isIncasso());
        $this->assertFalse($betaling->isIdeal());
    }

    /**
     * Test if betaling can be iDeal.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_ideal(): void
    {
        $betaling = create(Betaling::class, ['type' => 'ideal']);
        $this->assertTrue($betaling->isIDeal());
        $this->assertFalse($betaling->isIncasso());
    }

    /**
     * Test if a betaling can be open.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_open(): void
    {
        $betaling = create(Betaling::class, ['status' => 'open']);
        $this->assertTrue($betaling->isOpen());
        $this->assertFalse($betaling->isPaid());
    }

    /**
     * Test if a betaling can be paid.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_paid(): void
    {
        $betaling = create(Betaling::class, ['status' => 'paid']);
        $this->assertTrue($betaling->isPaid());
        $this->assertFalse($betaling->isOpen());
    }

    /**
     * Test if a betaling can be expired.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_expired(): void
    {
        $betaling = create(Betaling::class, ['status' => 'expired']);
        $this->assertTrue($betaling->isExpired());
        $this->assertFalse($betaling->isOpen());
    }

    /**
     * Test if a payment can be failed.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_failed(): void
    {
        $betaling = create(Betaling::class, ['status' => 'failed']);
        $this->assertTrue($betaling->isFailed());
        $this->assertFalse($betaling->isOpen());
    }

    /**
     * Test if a payment can be cancelled.
     *
     * @test
     *
     * @return void
     */
    public function a_betaling_can_be_canceled(): void
    {
        $betaling = create(Betaling::class, ['status' => 'canceled']);
        $this->assertTrue($betaling->isCanceled());
        $this->assertFalse($betaling->isOpen());
    }
}
