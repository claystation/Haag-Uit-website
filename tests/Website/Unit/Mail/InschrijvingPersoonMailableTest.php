<?php

namespace Tests\Website\Unit\Mail;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\InschrijvingPersoonMailable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

class InschrijvingPersoonMailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Inschrijving iDeal mailable test.
     *
     * @test
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function an_inschrijving_persoon_mailable_can_be_constructed_for_ideal(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $inschrijving = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        create(Betaling::class, ['type' => 'ideal', 'kamp_inschrijving_id' => $inschrijving->id]);
        $paymentUrl = 'https://www.mollie.com/payscreen/select-method/7UhSN1zuXS';
        $mailable = new InschrijvingPersoonMailable($inschrijving, $paymentUrl);

        $mailable->build();
        $this->assertContains([
            'name' => $inschrijving->persoon->voornaam.' '.$inschrijving->persoon->achternaam,
            'address' => $inschrijving->persoon->email,
        ], $mailable->to);
        $this->assertEquals("Inschrijving Haag Uit {$kampjaar->jaar}", $mailable->subject);
        $this->assertContains(['name' => 'Staf Haag Uit', 'address' => 'staf@haaguit.com'], $mailable->from);

        $rendered = $mailable->render();
        $this->assertStringContainsString("Beste {$inschrijving->persoon->roepnaam},", $rendered);
        $this->assertStringContainsString('Je hebt er voor gekozen om te betalen middels iDeal', $rendered);
        $this->assertStringContainsString($paymentUrl, $rendered);
    }

    /**
     * Inschrijving iDeal mailable test.
     *
     * @test
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function an_inschrijving_persoon_mailable_can_be_constructed_for_incasso(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $inschrijving = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        create(Betaling::class, ['type' => 'incasso', 'kamp_inschrijving_id' => $inschrijving]);
        $mailable = new InschrijvingPersoonMailable($inschrijving);

        $mailable->build();
        $this->assertContains([
            'name' => $inschrijving->persoon->voornaam.' '.$inschrijving->persoon->achternaam,
            'address' => $inschrijving->persoon->email,
        ], $mailable->to);
        $this->assertEquals("Inschrijving Haag Uit {$kampjaar->jaar}", $mailable->subject);
        $this->assertContains(['name' => 'Staf Haag Uit', 'address' => 'staf@haaguit.com'], $mailable->from);

        $rendered = $mailable->render();
        $this->assertStringContainsString("Beste {$inschrijving->persoon->roepnaam},", $rendered);
        $this->assertStringContainsString('Je hebt er voor gekozen om te betalen middels incasso', $rendered);
        $this->assertStringContainsString('€ '.$inschrijving->betalingen()->latest()->first()->bedrag, $rendered);
    }
}
