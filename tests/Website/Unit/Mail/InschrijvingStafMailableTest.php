<?php

namespace Tests\Website\Unit\Mail;

use Carbon\Carbon;
use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\InschrijvingStafMailable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

class InschrijvingStafMailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Inschrijving iDeal mailable test.
     *
     * @test
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function an_inschrijving_staf_mailable_can_be_constructed_for_ideal(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $inschrijving = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        create(Betaling::class, [
            'type'                 => 'ideal',
            'kamp_inschrijving_id' => $inschrijving->id,
        ]);
        $mailable = new InschrijvingStafMailable($inschrijving);

        $mailable->build();
        $this->assertContains(['name' => 'Staf Haag Uit', 'address' => 'staf@haaguit.com'], $mailable->to);
        $this->assertEquals("Nieuwe inschrijving voor Haag Uit {$kampjaar->jaar}", $mailable->subject);
        $this->assertContains(['name' => 'HUplicatie', 'address' => 'staf@haaguit.com'], $mailable->from);

        $rendered = $mailable->render();
        $this->assertStringContainsString(
            "{$inschrijving->persoon->voornaam} {$inschrijving->persoon->achternaam}",
            $rendered
        );
        $this->assertStringContainsString($inschrijving->persoon->email, $rendered);
        $this->assertStringContainsString($inschrijving->persoon->geslacht, $rendered);
        $this->assertStringContainsString($inschrijving->persoon->geboortedatum->format('d-m-Y'), $rendered);
        $this->assertStringContainsString('ideal', $rendered);
    }

    /**
     * Inschrijving Incasso mailable test.
     *
     * @test
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function an_inschrijving_staf_mailable_can_be_constructed_for_incasso(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $inschrijving = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        create(Betaling::class, [
            'type'                 => 'incasso',
            'kamp_inschrijving_id' => $inschrijving->id,
        ]);
        $mailable = new InschrijvingStafMailable($inschrijving);

        $mailable->build();
        $this->assertContains(['name' => 'Staf Haag Uit', 'address' => 'staf@haaguit.com'], $mailable->to);
        $this->assertEquals("Nieuwe inschrijving voor Haag Uit {$kampjaar->jaar}", $mailable->subject);
        $this->assertContains(['name' => 'HUplicatie', 'address' => 'staf@haaguit.com'], $mailable->from);

        $rendered = $mailable->render();
        $this->assertStringContainsString(
            "{$inschrijving->persoon->voornaam} {$inschrijving->persoon->achternaam}",
            $rendered
        );
        $this->assertStringContainsString($inschrijving->persoon->email, $rendered);
        $this->assertStringContainsString($inschrijving->persoon->geslacht, $rendered);
        $this->assertStringContainsString(
            Carbon::parse($inschrijving->persoon->geboortedatum)->format('d-m-Y'),
            $rendered
        );
        $this->assertStringContainsString('incasso', $rendered);
    }
}
