<?php

namespace Tests\Website\Unit\Mail;

use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\ContactRequestMailable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

class ContactRequestMailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Contact mailable test.
     *
     * @test
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function a_contact_request_mailable_can_be_constructed(): void
    {
        create(Kampjaar::class, ['actief' => 1]);
        $mailable = new ContactRequestMailable('Baccanardo', 'bacca@nardo.nl', 'Het antwoord op je vraag is JA.');

        $mailable->build();
        $this->assertContains(['name' => 'Staf Haag Uit', 'address' => 'staf@haaguit.com'], $mailable->to);
        $this->assertEquals('Het antwoord op je vraag is JA.', $mailable->text);

        $rendered = $mailable->render();
        $this->assertMatchesRegularExpression(
            '/Er is een vraag via de website van Baccanardo \(bacca@nardo\.nl\):/',
            $rendered
        );
        $this->assertMatchesRegularExpression('/Het antwoord op je vraag is JA./', $rendered);
    }
}
