<?php

namespace Tests\Website\Unit;

use Carbon\Carbon;
use HUplicatie\Http\ViewComposers\KampjaarComposer;
use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\View\View;
use Mockery;
use Tests\TestCase;

class KampjaarComposerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a year can be retrieved.
     *
     * @test
     *
     * @return void
     */
    public function a_year_can_be_retrieved(): void
    {
        create(Kampjaar::class, ['actief' => 1]);

        $kampjaarComposer = new KampjaarComposer();
        $view = Mockery::mock(View::class);
        $view->shouldReceive('with')->with('kampjaar', Mockery::type(Kampjaar::class));
        $kampjaarComposer->compose($view);
    }

    /**
     * Test if the current year is used when no other kampjaar exists.
     *
     * @test
     */
    public function the_current_year_is_used_when_no_active_kampjaar_exists(): void
    {
        $kampjaarComposer = new KampjaarComposer();
        $view = Mockery::mock(View::class);
        $view->shouldReceive('with')->withArgs(function ($string, $kampjaar) {
            return $string === 'kampjaar' && $kampjaar->jaar === Carbon::now()->year;
        });
        $kampjaarComposer->compose($view);
    }
}
