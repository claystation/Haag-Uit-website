<?php

namespace Tests\Website\Feature\Betaling;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mollie\Api\Exceptions\IncompatiblePlatform;
use Mollie\Api\Types\PaymentStatus;
use Tests\TestCase;
use Tests\utilities\MollieMockResponseFactory;
use Tests\WorksWithMollie;

class RetryPaymentTest extends TestCase
{
    use RefreshDatabase, WorksWithMollie;

    /**
     * Test if a request returns a 404 without an id.
     *
     * @test
     *
     * @return void
     */
    public function a_request_returns_404_without_an_id(): void
    {
        $this->get('/betalingen/opnieuw')
            ->assertRedirect('/');
    }

    /**
     * Test if a payment cannot be retried when no prior payment exists.
     *
     * @test
     *
     * @return void
     */
    public function a_payment_cannot_be_retried_when_no_prior_payment_exists(): void
    {
        $kampInschrijving = create(KampInschrijving::class);
        $this->assertCount(0, $kampInschrijving->betalingen);
        $this->get("/betalingen/opnieuw?id={$kampInschrijving->id}")
            ->assertStatus(200)
            ->assertSee('Neem contact op met de staf!');
    }

    /**
     * Test that an incasso payment cannot be retried.
     *
     * @test
     *
     * @return void
     */
    public function an_incasso_payment_cannot_be_retried(): void
    {
        create(Betaling::class, [
            'type'                 => 'incasso',
            'transactie_id'        => '7292987979',
            'status'               => 'open',
            'kamp_inschrijving_id' => create(KampInschrijving::class, ['id' => 1]),
        ]);
        $this->assertCount(1, KampInschrijving::find(1)->betalingen);

        $this->get('/betalingen/opnieuw?id=1')
            ->assertRedirect('/');
    }

    /**
     * Test if a payment can be retried when not open or paid.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function a_payment_can_be_retried_when_not_open_or_paid(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->amount($amount = '103.96')
            ->paymentUrl($paymentUrl = 'https://lekkerbetalen.com')
            ->build();
        $responseStack->append($response);
        $betaling = create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => 'tr_987979',
            'bedrag'               => $amount,
            'status'               => PaymentStatus::STATUS_EXPIRED,
            'kamp_inschrijving_id' => create(KampInschrijving::class, ['id' => 1]),
        ]);

        $this->assertCount(1, KampInschrijving::find(1)->betalingen);

        $this->get('/betalingen/opnieuw?id=1')
            ->assertStatus(303)
            ->assertRedirect($paymentUrl);

        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertCount(2, KampInschrijving::find(1)->betalingen);

        $this->assertDatabaseHas('betalingen', [
            'type'          => 'ideal',
            'transactie_id' => $transactieId,
            'bedrag'        => $betaling->bedrag,
            'status'        => 'open',
        ]);
    }

    /**
     * Test if a user gets redirected when the payment is still open.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function a_user_gets_redirected_to_the_payment_when_payment_is_still_open(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->paymentUrl($paymentUrl = 'https://lekkerbetalen.com')
            ->build();
        $responseStack->append($response);
        create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => $transactieId,
            'status'               => PaymentStatus::STATUS_OPEN,
            'kamp_inschrijving_id' => create(KampInschrijving::class, ['id' => 1]),
        ]);
        $this->assertCount(1, KampInschrijving::find(1)->betalingen);

        $this->get('/betalingen/opnieuw?id=1')
            ->assertStatus(303)
            ->assertRedirect($paymentUrl);

        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertCount(1, KampInschrijving::find(1)->betalingen);
    }

    /**
     * Test if text is returned when payment is already payed.
     *
     * @test
     *
     * @return void
     */
    public function text_is_returned_when_the_payment_is_already_paid(): void
    {
        create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => 'tr_123131',
            'status'               => PaymentStatus::STATUS_PAID,
            'kamp_inschrijving_id' => create(KampInschrijving::class, ['id' => 1]),
        ]);
        $this->assertCount(1, KampInschrijving::find(1)->betalingen);

        $this->get('/betalingen/opnieuw?id=1')
            ->assertStatus(200)
            ->assertSeeText('Je betaling is goed gegaan en afgerond');

        $this->assertCount(0, $this->mollieRequestHistory);
        $this->assertCount(1, KampInschrijving::find(1)->betalingen);
    }
}
