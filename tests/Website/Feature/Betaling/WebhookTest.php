<?php

namespace Tests\Website\Feature\Betaling;

use HUplicatie\Betaling;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mollie\Api\Exceptions\IncompatiblePlatform;
use Tests\TestCase;
use Tests\utilities\MollieMockResponseFactory;
use Tests\WorksWithMollie;

class WebhookTest extends TestCase
{
    use RefreshDatabase, WorksWithMollie;

    /**
     * Test if requests without id a 404 is returned.
     *
     * @test
     */
    public function when_no_id_is_provided_404_is_returned_from_the_webhook(): void
    {
        $this->json('POST', route('paymentsWebhook'))
            ->assertStatus(404);
    }

    /**
     * Test when an invalid id is given the webhook returns an error.
     *
     * @test
     */
    public function when_an_invalid_id_is_provided_an_error_is_returned_from_the_webhook(): void
    {
        $this->json('POST', route('paymentsWebhook', ['id' => 'something malicious!']))
            ->assertStatus(400);
    }

    /**
     * Test if a webhook can be called with a successful payment.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function the_webhook_can_be_called_with_a_successful_payment(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->paid()
            ->details('Petertje Metertje', 'NL12INGB0123456789')
            ->build();
        $responseStack->append($response);

        $betaling = create(Betaling::class, [
            'transactie_id' => $transactieId,
            'status'        => 'open',
        ]);

        $this->post(route('paymentsWebhook'), ['id' => $transactieId])
            ->assertStatus(200);

        $betaling->refresh();
        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertTrue($betaling->isPaid());
        $this->assertEquals('Petertje Metertje', $betaling->rekeninghouder_naam);
        $this->assertEquals('NL12INGB0123456789', $betaling->rekeninghouder_nummer);
    }

    /**
     * Test if the webhook can be called with an expired payment.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function the_webhook_can_be_called_with_an_expired_payment(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->expired()
            ->build();
        $responseStack->append($response);

        $betaling = create(Betaling::class, [
            'transactie_id'         => $transactieId,
            'status'                => 'open',
            'rekeninghouder_naam'   => null,
            'rekeninghouder_nummer' => null,
        ]);

        $this->post(route('paymentsWebhook'), ['id' => $transactieId])
            ->assertStatus(200);

        $betaling->refresh();
        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertTrue($betaling->isExpired());
        $this->assertNull($betaling->rekeninghouder_naam);
        $this->assertNull($betaling->rekeninghouder_nummer);
    }

    /**
     * Test if the webhook can be called with a failed payment.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function the_webhook_can_be_called_with_a_failed_payment(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->failed()
            ->build();
        $responseStack->append($response);

        $betaling = create(Betaling::class, [
            'transactie_id'         => $transactieId,
            'status'                => 'open',
            'rekeninghouder_naam'   => null,
            'rekeninghouder_nummer' => null,
        ]);

        $this->post(route('paymentsWebhook'), ['id' => $transactieId])
            ->assertStatus(200);

        $betaling->refresh();
        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertTrue($betaling->isFailed());
        $this->assertNull($betaling->rekeninghouder_naam);
        $this->assertNull($betaling->rekeninghouder_nummer);
    }

    /**
     * Test if the webhook returns a 400 when an unknown id is provided.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function the_webhook_returns_400_when_an_unknown_id_is_provided(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->errorResponse(
                404,
                '404',
                'Not Found',
                'No payment exists with token tr_123131.'
            )
            ->build();
        $responseStack->append($response);

        $this->post(route('paymentsWebhook'), ['id' => 'tr_123131'])
            ->assertStatus(400);

        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertCount(0, Betaling::all());
    }
}
