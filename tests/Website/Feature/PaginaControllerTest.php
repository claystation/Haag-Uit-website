<?php

namespace Tests\Website\Feature;

use HUplicatie\Kampjaar;
use HUplicatie\Pagina;
use HUplicatie\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaginaControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        create(Kampjaar::class, ['actief' => 1]);
    }

    /**
     * Test if the default Pagina is the home page.
     *
     * @test
     */
    public function the_default_page_is_the_home_page(): void
    {
        $pagina = Pagina::where('naam', 'home')->first();
        $promo = Pagina::where('naam', 'promo')->first();
        $quote = create(Quote::class);
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee($pagina->titel);
        $response->assertSee($quote->tekst);
        $response->assertSee(e($promo->titel));
        $response->assertSee($promo->tekst);
    }

    /**
     * Test if any other Pagina can be visited.
     *
     * @test
     */
    public function any_other_page_can_be_visited(): void
    {
        $pagina = Pagina::where('naam', 'av')->first();
        $response = $this->get('/pagina/av');

        $response->assertStatus(200);
        $response->assertSee($pagina->titel);
        $response->assertSee($pagina->tekst);
    }

    /**
     * Test if a non existent url redirects to the home page.
     *
     * @test
     */
    public function a_non_existent_url_redirects_to_the_home_page(): void
    {
        $this->withExceptionHandling();
        $response = $this->get('/nonexistanturl');

        $response->assertStatus(404);
        $response->assertSee('Deze pagina bestaat helaas niet (meer).');
    }

    /**
     * Test if a non existent Pagina redirects to the home page.
     *
     * @test
     */
    public function a_non_existent_page_redirects_to_the_home_page(): void
    {
        $response = $this->get('/pagina/nonexistantpage');

        $response->assertRedirect('/');
    }
}
