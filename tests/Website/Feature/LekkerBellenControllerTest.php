<?php

namespace Tests\Website\Feature;

use HUplicatie\Authorization\Roles;
use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class LekkerBellenControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        app(KampjaarFactory::class)->withInschrijvingen(111)->create(2018);
    }

    /**
     * Test if a user must be authenticated to visit lekkerbellen.
     *
     * @test
     *
     * @return void
     */
    public function user_must_be_authenticated_to_visit_lekker_bellen(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->withExceptionHandling()
            ->get('/lekkerbellen')
            ->assertRedirect('/stafplicatie/login');

        $this->signInAsRole(Roles::INSCHRIJVING);
        $this->withExceptionHandling()
            ->get('/lekkerbellen')
            ->assertStatus(403);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->withExceptionHandling()
            ->get('/lekkerbellen')
            ->assertStatus(200);

        $this->signInAsRole(Roles::STAFFER);
        $this->withExceptionHandling()
            ->get('/lekkerbellen')
            ->assertStatus(200);
    }

    /**
     * Test if lekkerbellen can be viewed.
     *
     * @test
     */
    public function lekker_bellen_can_be_viewed(): void
    {
        app(KampjaarFactory::class)->isActief()->withInschrijvingen(30)->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $response = $this->get('/lekkerbellen');

        $response->assertStatus(200)
            ->assertSee('Hallo Haag Uit!')
            ->assertSee(2019);
    }

    /**
     * Test if first ratio is displayed.
     *
     * @test
     */
    public function at_total_1_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('Paaltjes')
            ->assertSeeText('Volgende op 1!');

        create(KampInschrijving::class, ['jaar' => 2019], 1);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSeeText('Paaltjes')
            ->assertDontSeeText('Volgende op 1!');
    }

    /**
     * Test if second ratio is displayed.
     *
     * @test
     */
    public function at_total_10_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('Man:')
            ->assertSeeText('Volgende op 10');

        create(KampInschrijving::class, ['jaar' => 2019], 10);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSeeText('Man:')
            ->assertDontSeeText('Volgende op 10 of meer');
    }

    /**
     * Test if third ratio is displayed.
     *
     * @test
     */
    public function at_total_25_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('CMD:')
            ->assertSeeText('Volgende op 25');

        $inschrijvingen = create(KampInschrijving::class, ['jaar' => 2019], 25);
        $cmd = $inschrijvingen->filter(function ($value) {
            return $value->persoon->studie->naam === 'CMD';
        })->count();
        $hbo = $inschrijvingen->filter(function ($value) {
            return $value->persoon->studie->naam === 'HBO-ICT';
        })->count();
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSeeText("CMD: $cmd")
            ->assertSeeText("HBO-ICT:\n            $hbo")
            ->assertDontSeeText('Volgende op 25');
    }

    /**
     * Test if fourth ratio is displayed.
     *
     * @test
     */
    public function at_total_40_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('Meerderjarig:')
            ->assertSeeText('Volgende op 40');

        create(KampInschrijving::class, ['jaar' => 2019], 40);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertStatus(200)
            ->assertSeeText('Meerderjarig:')
            ->assertDontSeeText('Volgende op 40');
    }

    /**
     * Test if fifth ratio is displayed.
     *
     * @test
     */
    public function at_total_70_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('DTF:')
            ->assertSeeText('Volgende op 70');

        create(KampInschrijving::class, ['jaar' => 2019], 70);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSeeText('DTF')
            ->assertDontSeeText('Volgende op 70');
    }

    /**
     * Test if sixth ratio is displayed.
     *
     * @test
     */
    public function at_total_80_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSee('airhorn-component')
            ->assertSeeText('AIRHORN OP 80!');

        create(KampInschrijving::class, ['jaar' => 2019], 80);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSee('airhorn-component')
            ->assertDontSeeText('AIRHORN OP 80!');
    }

    /**
     * Test if seventh ratio is displayed.
     *
     * @test
     */
    public function at_total_100_first_ratio_is_displayed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->signInAsRole(Roles::MEDEWERKER);
        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertDontSeeText('Vandaag op deze dag in')
            ->assertSeeText('Volgende op 100');

        create(KampInschrijving::class, ['jaar' => 2019], 100);

        $this->get('/lekkerbellen')
            ->assertStatus(200)
            ->assertSeeText('Vandaag op deze dag in')
            ->assertDontSeeText('Volgende op 100');
    }
}
