<?php

namespace Tests\Website\Feature\Registratie;

use Carbon\Carbon;
use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\InschrijvingPersoonMailable;
use HUplicatie\Mail\Website\InschrijvingStafMailable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Tests\utilities\MollieMockResponseFactory;
use Tests\WorksWithGoogleCaptcha;

class RegistratieSuccessControllerTest extends TestCase
{
    use RefreshDatabase, TestsRegistration, WorksWithGoogleCaptcha;

    private $transactieId;
    private $paymentUrl;
    private $response;

    protected function setUp(): void
    {
        parent::setUp();
        Mail::fake();
        $this->response = (new MollieMockResponseFactory())
            ->transactieId($this->transactieId = 'tr_123131')
            ->amount('100.00')
            ->paymentUrl($this->paymentUrl = 'https://lekkerbetalen.com')
            ->build();
    }

    /**
     * Test if the registration page can be viewed.
     *
     * @test
     */
    public function the_registration_page_can_be_viewed(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);

        $this->get('/inschrijven')
            ->assertStatus(200)
            ->assertSee("Inschrijven voor Haag Uit $kampjaar->jaar");
    }

    /**
     * Test if the registration request can be submitted with iDeal.
     *
     * @test
     */
    public function a_registration_request_can_be_submitted_with_iDeal(): void
    {
        $this->mockCaptchaVerify();
        $responseStack = $this->mockMollie();
        $responseStack->append($this->response);
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);

        $this->submitRegistration()
            ->assertRedirect($this->paymentUrl)
            ->assertStatus(302);

        $this->assertDatabaseHas('personen', [
            'voornaam'      => $this->formData['voornaam'],
            'achternaam'    => $this->formData['achternaam'],
            'geboortedatum' => '2000-05-26 00:00:00',
        ]);

        $this->assertDatabaseHas('kamp_inschrijvingen', [
            'opmerking'              => $this->formData['opmerking'],
            'kledingmaat'            => $this->formData['kledingmaat'],
            'annuleringsverzekering' => '0',
            'jaar'                   => Kampjaar::getOpenKampjaarYear(),
        ]);

        $this->assertDatabaseHas('betalingen', [
            'type'          => 'ideal',
            'transactie_id' => $this->transactieId,
            'bedrag'        => $kampjaar->inschrijfbedrag,
            'status'        => 'open',
        ]);
        $this->assertCount(1, $this->mollieRequestHistory);
        Mail::assertQueued(InschrijvingPersoonMailable::class, function ($mail) {
            return $this->paymentUrl === $mail->paymentUrl;
        });
        Mail::assertQueued(InschrijvingStafMailable::class);
    }

    /**
     * Test if a registration request can be submitted with an incasso.
     *
     * @test
     */
    public function a_registration_request_can_be_submitted_with_incasso(): void
    {
        $this->mockCaptchaVerify();

        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);

        $this->submitRegistration([], false, true)
            ->assertStatus(200)
            ->assertSee('Inschrijving gelukt!')
            ->assertSee($this->formData['email'])
            ->assertSee(Carbon::parse($kampjaar->start)->subWeek(2)->format('d-m-Y'))
            ->assertDontSee($this->paymentUrl);

        $this->assertDatabaseHas('personen', [
            'voornaam'      => $this->formData['voornaam'],
            'achternaam'    => $this->formData['achternaam'],
            'geboortedatum' => '2000-05-26 00:00:00',
        ]);

        $this->assertDatabaseHas('kamp_inschrijvingen', [
            'opmerking'              => $this->formData['opmerking'],
            'kledingmaat'            => $this->formData['kledingmaat'],
            'annuleringsverzekering' => '0',
            'jaar'                   => Kampjaar::getOpenKampjaarYear(),
        ]);

        $this->assertDatabaseHas('betalingen', [
            'type'                      => 'incasso',
            'bedrag'                    => $kampjaar->inschrijfbedrag,
            'status'                    => 'open',
            'rekeninghouder_naam'       => 'Baccanardo',
            'rekeninghouder_nummer'     => 'NL07ABNA1231156754',
            'rekeninghouder_woonplaats' => 'Den Haag',
        ]);
        Mail::assertQueued(InschrijvingPersoonMailable::class, function ($mail) {
            return $mail->paymentUrl === null;
        });
        Mail::assertQueued(InschrijvingStafMailable::class);
    }

    /**
     * Test if a registration can have insurance.
     *
     * @test
     */
    public function a_registration_can_have_insurance(): void
    {
        $this->mockCaptchaVerify();

        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);
        $responseStack = $this->mockMollie();
        $responseStack->append(
            (new MollieMockResponseFactory())
            ->transactieId($this->transactieId = 'tr_123131')
            ->amount($kampjaar->getAmountWithInsurance())
            ->paymentUrl($this->paymentUrl = 'https://lekkerbetalen.com')
            ->build()
        );

        $this->submitRegistration([], true)
            ->assertRedirect($this->paymentUrl)
            ->assertStatus(302);

        $this->assertCount(1, $this->mollieRequestHistory);
        $requestBody = $this->getRequestBodyFromHistory(1);
        $this->assertEquals($kampjaar->getAmountWithInsurance(), $requestBody->amount->value);

        $this->assertDatabaseHas('personen', [
            'voornaam'   => $this->formData['voornaam'],
            'achternaam' => $this->formData['achternaam'],
        ]);

        $this->assertDatabaseHas('kamp_inschrijvingen', [
            'opmerking'              => $this->formData['opmerking'],
            'kledingmaat'            => $this->formData['kledingmaat'],
            'annuleringsverzekering' => '1',
        ]);

        $this->assertDatabaseHas('betalingen', [
            'type'          => 'ideal',
            'transactie_id' => $this->transactieId,
            'bedrag'        => $kampjaar->getAmountWithInsurance(),
            'status'        => 'open',
        ]);
        Mail::assertQueued(InschrijvingPersoonMailable::class, function ($mail) {
            return $this->paymentUrl === $mail->paymentUrl;
        });
        Mail::assertQueued(InschrijvingStafMailable::class);
    }

    /**
     * Test if a registration can only be submitted when the registration is open.
     *
     * @test
     */
    public function a_registration_can_only_be_submitted_when_registration_is_open(): void
    {
        $this->mockCaptchaVerify();

        $responseStack = $this->mockMollie();
        $responseStack->append($this->response);
        create(Kampjaar::class, ['actief' => 1, 'open' => 1]);

        $this->submitRegistration()
            ->assertRedirect($this->paymentUrl)
            ->assertStatus(302);

        $this->assertCount(1, $this->mollieRequestHistory);
        Mail::assertQueued(InschrijvingPersoonMailable::class, function ($mail) {
            return $this->paymentUrl === $mail->paymentUrl;
        });
        Mail::assertQueued(InschrijvingStafMailable::class);
    }
}
