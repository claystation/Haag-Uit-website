<?php

namespace Tests\Website\Feature\Registratie;

use Illuminate\Foundation\Testing\TestResponse;
use Tests\WorksWithMollie;

trait TestsRegistration
{
    use WorksWithMollie;

    protected $mockHandler;

    public $formData = [
        'av' => '1',
        'roepnaam' => 'Clem',
        'voornaam' => 'Baccanardo',
        'voorletters' => 'C.',
        'achternaam' => 'Baccanardo',
        'geboortedatum' => '26-05-2000',
        'geslacht' => 'm',
        'studie_id' => '1',
        'kledingmaat' => 'm',
        'opmerking' => 'Doe maar metertje',
        'straat' => 'kerklaan',
        'huisnummer' => '10',
        'postcode' => '1234RT',
        'woonplaats' => 'Den Haag',
        'email' => 'mail@mail.com',
        'mobiel' => '0612312312',
        'telefoon_nood' => '0612312312',
        'type' => 'ideal',
    ];

    /**
     * Submits a registration.
     *
     * @param  array  $overrides
     * @param  bool  $withInsurance
     * @param  bool  $isIncasso
     * @return TestResponse
     */
    protected function submitRegistration(
        array $overrides = [],
        $withInsurance = false,
        $isIncasso = false
    ): TestResponse {
        if ($withInsurance) {
            $this->formData['annuleringsverzekering'] = '1';
        }

        if ($isIncasso) {
            $this->formData['type'] = 'incasso';
            $this->formData['rekeninghouder_naam'] = 'Baccanardo';
            $this->formData['rekeninghouder_nummer'] = 'NL07ABNA1231156754';
            $this->formData['rekeninghouder_woonplaats'] = 'Den Haag';
        }

        $data = array_replace_recursive($this->formData, $overrides);

        return $this->post('/inschrijven', $data);
    }

    protected function withDefaultMollieMockResponse()
    {
        $this->mockHandler = $this->mockHandler ?? $this->mockMollie();
        $this->mockHandler->append($this->createStandardMollieResponse());

        return $this;
    }
}
