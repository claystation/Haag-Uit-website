<?php

namespace Tests\Website\Feature\Registratie;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class RegistratieClosedControllerTest extends TestCase
{
    use RefreshDatabase, TestsRegistration;

    protected function setUp(): void
    {
        parent::setUp();
        Mail::fake();
    }

    /**
     * Test if the registration form is not shown when registration is not open.
     *
     * @test
     */
    public function the_registration_form_cannot_be_viewed_when_the_registration_is_not_open(): void
    {
        create(Kampjaar::class, ['actief' => 1, 'open' => 0]);
        $this->get('/inschrijven')
            ->assertStatus(302)
            ->assertRedirect('/inschrijven/gesloten');

        $this->followingRedirects()
            ->get('/inschrijven')
            ->assertStatus(200)
            ->assertSee('Inschrijvingen gesloten');
    }

    /**
     * Test if the registration form cannot be viewed when there is no active kampjaar.
     *
     * @test
     */
    public function the_registration_form_cannot_be_viewed_when_there_is_no_active_kampjaar(): void
    {
        create(Kampjaar::class, ['actief' => 0]);

        $this->get('/inschrijven')
            ->assertStatus(302)
            ->assertRedirect('/inschrijven/gesloten');
    }

    /**
     * Test if a registration cannot be submitted when the registration is not open.
     *
     * @test
     */
    public function a_registration_cannot_be_submitted_when_registration_is_not_open(): void
    {
        create(Kampjaar::class, ['actief' => 1, 'open' => 0]);

        $this->submitRegistration()
            ->assertStatus(302)
            ->assertRedirect('/inschrijven/gesloten');
        Mail::assertNothingSent();
    }
}
