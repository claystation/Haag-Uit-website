<?php

namespace Tests\Website\Feature;

use Carbon\Carbon;
use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NieuwsFeedControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if the latest 5 Nieuwsberichten can be fetched.
     *
     * @test
     */
    public function the_five_latest_news_articles_can_be_fetched(): void
    {
        $old = create(Nieuwsbericht::class, [
            'created_at' => Carbon::now()->subDay(),
            'updated_at' => Carbon::now()->subDay(),
        ]);
        $nieuws = create(Nieuwsbericht::class, [], 5);

        $response = $this->getJson('/api/nieuws/feed?count=5');
        $response->assertStatus(200)
            ->assertJsonMissing($old->toArray())
            ->assertJsonCount(5)
            ->assertJson($nieuws->toArray());
    }

    /**
     * Test if the latest 5 Nieuwsberichten will be fetched when no count is given.
     *
     * @test
     */
    public function the_five_latest_news_articles_will_be_fetched_when_no_count_is_given(): void
    {
        $old = create(Nieuwsbericht::class, [
            'created_at' => Carbon::now()->subDay(),
            'updated_at' => Carbon::now()->subDay(),
        ]);
        $nieuws = create(Nieuwsbericht::class, [], 5);

        $response = $this->getJson('/api/nieuws/feed');
        $response->assertStatus(200)
            ->assertJsonCount(5)
            ->assertJsonMissing($old->toArray())
            ->assertJson($nieuws->toArray());
    }

    /**
     * Test if the latest 6 Nieuwsberichten can be fetched with a count of 6.
     *
     * @test
     */
    public function the_six_latest_news_articles_can_be_fetched_with_a_count(): void
    {
        $old = create(Nieuwsbericht::class, ['created_at' => Carbon::now()->subDay()]);
        $nieuws = create(Nieuwsbericht::class, [], 5);

        $response = $this->getJson('/api/nieuws/feed?count=6');
        $response->assertStatus(200)
            ->assertJsonCount(6)
            ->assertJsonFragment($old->toArray())
            ->assertJson($nieuws->toArray());
    }

    /**
     * Test if all Nieuwsberichten will be returned when the requested count is bigger
     * than the total of Nieuwsberichten.
     *
     * @test
     */
    public function the_max_latest_news_articles_can_be_fetched_when_the_count_is_bigger_than_total(): void
    {
        $nieuws = create(Nieuwsbericht::class, [], 5);

        $response = $this->getJson('/api/nieuws/feed?count=6');
        $response->assertStatus(200)
            ->assertJsonCount(5)
            ->assertJson($nieuws->toArray());
    }
}
