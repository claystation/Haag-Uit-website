<?php

namespace Tests\utilities;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use Mollie\Api\Types\PaymentStatus;

class MollieMockResponseFactory
{
    private $data;
    private $status;

    /**
     * MollieMockResponseFactory constructor.
     */
    public function __construct()
    {
        $this->status = 200;
        $this->data = [];
    }

    public function errorResponse(
        int $statusCode,
        string $status,
        string $title,
        string $detail
    ): MollieMockResponseFactory {
        $this->status = $statusCode;
        $this->data['status'] = $status;
        $this->data['title'] = $title;
        $this->data['detail'] = $detail;

        return $this;
    }

    /**
     * Sets the transaction Id.
     *
     * @param  string  $transactionId
     * @return $this
     */
    public function transactieId(string $transactionId): self
    {
        $this->data['id'] = $transactionId;

        return $this;
    }

    public function paymentUrl(string $paymentUrl): MollieMockResponseFactory
    {
        if (! Arr::has($this->data, '_links.checkout')) {
            $this->data['_links'] = ['checkout' => ['href' => $paymentUrl]];
        } else {
            $this->data['_links']['checkout']['href'] = $paymentUrl;
        }

        return $this;
    }

    public function amount(string $amount): MollieMockResponseFactory
    {
        $this->data['amount'] = ['currency' => 'EUR', 'value' => $amount];

        return $this;
    }

    public function paid(): MollieMockResponseFactory
    {
        $this->data['status'] = PaymentStatus::STATUS_PAID;
        $this->data['paidAt'] = Carbon::now()->toIso8601String();

        return $this;
    }

    public function expired(): MollieMockResponseFactory
    {
        $this->data['status'] = PaymentStatus::STATUS_EXPIRED;

        return $this;
    }

    public function failed(): MollieMockResponseFactory
    {
        $this->data['status'] = PaymentStatus::STATUS_FAILED;

        return $this;
    }

    public function details(string $name, string $account): MollieMockResponseFactory
    {
        if (! Arr::has($this->data, 'details')) {
            $this->data['details'] = [
                'consumerName'    => $name,
                'consumerAccount' => $account,
            ];
        } else {
            $this->data['details']['consumerName'] = $name;
            $this->data['details']['consumerAccount'] = $account;
        }

        return $this;
    }

    /**
     * Returns a new Mollie Mock Response.
     *
     * @return Response
     */
    public function build(): Response
    {
        return new Response($this->status, [], json_encode($this->data));
    }
}
