<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    use SoftDeletes;

    protected $table = 'sponsoren';

    protected $fillable = ['naam', 'logo', 'url'];

    public function getLogoAttribute($logo): string
    {
        return asset('/storage/'.$logo);
    }
}
