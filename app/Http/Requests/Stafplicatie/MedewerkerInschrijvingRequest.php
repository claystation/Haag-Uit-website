<?php

namespace HUplicatie\Http\Requests\Stafplicatie;

use Illuminate\Foundation\Http\FormRequest;

class MedewerkerInschrijvingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'voornaam' => 'required|string',
            'achternaam' => 'required|string',
            'voorletters' => 'string|nullable',
            'roepnaam' => 'string|nullable',
            'straat' => 'required|string',
            'huisnummer' => 'required|string',
            'postcode' => 'required|string',
            'woonplaats' => 'required|string',
            'mobiel' => 'required|string',
            'telefoon_nood' => 'required|string',
            'studentnummer' => 'required|string',
            'geboortedatum' => 'required|date',
            'email' => 'required|email',
            'geslacht' => 'required',
            'rijbewijs' => 'required',
            'ehbo' => 'required',
            'studie_id' => 'required|integer',
            'eerste_keus' => 'required|array',
            'eerste_keus.*' => 'exists:functies,naam',
            'tweede_keus' => 'present|array',
            'tweede_keus.*' => 'exists:functies,naam',
            'ervaring' => 'nullable|string',
            'motivatie' => 'nullable|string',
            'eigenschappen' => 'nullable|string',
        ];
    }
}
