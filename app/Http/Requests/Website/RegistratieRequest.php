<?php

namespace HUplicatie\Http\Requests\Website;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;

class RegistratieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'av' => 'accepted',
            'annuleringsverzekering' => 'sometimes|accepted',
            'roepnaam' => ['required', 'regex:/^[a-zA-ZÀ-ž0-9\s\-]+$/', 'max:30'],
            'voornaam' => ['required', 'regex:/^[a-zA-ZÀ-ž0-9\s\-]+$/', 'max:30'],
            'voorletters' => ['required', 'regex:/[A-Z]+(\.|\s)?/i', 'max:10'],
            'achternaam' => ['required', 'regex:/^[a-zA-ZÀ-ž0-9\s\-]+$/', 'max:30'],
            'geboortedatum' => 'required|date',
            'geslacht' => ['required', Rule::in(['m', 'v', 'nb'])],
            'studie_id' => ['required', 'integer', 'exists:studies,id'],
            'kledingmaat' => ['required', Rule::in(['xs', 's', 'm', 'l', 'xl'])],
            'opmerking' => 'max:750',
            'straat' => ['required', 'regex:/^[a-zA-ZÀ-ž0-9\s\-]+$/', 'max:50'],
            'huisnummer' => 'required|alpha_num|max:10',
            'postcode' => ['required', 'regex:/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i'],
            'woonplaats' => 'required|max:75',
            'email' => 'required|email|max:75',
            'mobiel' => ['required', 'regex:/^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/i'],
            'telefoon_nood' => ['required', 'regex:/^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/i'],
            'type' => ['required', Rule::in(['ideal', 'incasso'])],
            'g-recaptcha-response' => [
                new GoogleReCaptchaV3ValidationRule('registratie'),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            '*.required' => 'Het :attribute veld is verplicht.',
            '*.max' => 'Het :attribute veld mag maximaal :max karakters bevatten.',
            '*.alpha_dash' => 'Het :attribute veld mag alleen letters, cijfers en streepjes bevatten.',
            '*.alpha_num' => 'Het :attribute veld mag alleen letters en cijfers bevatten.',
            'roepnaam.regex' => 'Het :attribute veld bevat ongeldige tekens. '.
                'Gebruik alleen letters, cijfers, spaties en streepjes.',
            'voorletters.regex' => 'Deze voorletters zijn niet geldig. Formaat: A.B.C.',
            'voornaam.regex' => 'Het :attribute veld bevat ongeldige tekens.'.
                ' Gebruik alleen letters, cijfers, spaties en streepjes.',
            'achternaam.regex' => 'Het :attribute veld bevat ongeldige tekens.'.
                ' Gebruik alleen letters, cijfers, spaties en streepjes.',
            'straat.regex' => 'Deze straatnaam is niet geldig.'.
                ' Gebruik alleen letters, cijfers, spaties en streepjes.',
            'postcode.regex' => 'Deze postcode is niet geldig. (1234AB of 1234 AB)',
            'mobiel.regex' => 'Dit mobiele nummer is niet geldig.',
            'telefoon_nood.regex' => 'Dit mobiele nummer is niet geldig.',
            'email.email' => 'Dit email adres is niet geldig.',
        ];
    }

    /**
     * Sets the custom validation rules on the validator.
     *
     * @param  Validator  $validator
     */
    public function withValidator(Validator $validator): void
    {
        $validator->sometimes('rekeninghouder_naam', 'required', function ($input) {
            return $input->type === 'incasso';
        });

        $validator->sometimes('rekeninghouder_nummer', 'required', function ($input) {
            return $input->type === 'incasso';
        });

        $validator->sometimes('rekeninghouder_woonplaats', 'required', function ($input) {
            return $input->type === 'incasso';
        });
    }
}
