<?php

namespace HUplicatie\Http\Controllers\Website;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Pagina;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class PaginaController extends Controller
{
    /**
     * Return overview of all Pagina's.
     *
     * @return Factory|View
     */
    public function index()
    {
        $pagina = Pagina::where('naam', 'home')->first()->withVariableReplacements();
        $promo = Pagina::where('naam', 'promo')->first()->withVariableReplacements();

        return view('website.pages.index', compact(['pagina', 'promo']));
    }

    /**
     * Return overview of one Pagina.
     *
     * @param  string  $naam
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function show($naam = 'home')
    {
        $pagina = Pagina::where('naam', $naam)->first();
        if (! $pagina) {
            return redirect('/');
        }
        $pagina = $pagina->withVariableReplacements();

        return view('website.pages.show', compact('pagina'));
    }
}
