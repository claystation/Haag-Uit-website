<?php

namespace HUplicatie\Http\Controllers\Website\Api;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SponsorFeedController extends Controller
{
    /**
     * Returns collection of all sponsors.
     *
     * @return Collection
     */
    public function index(): Collection
    {
        return Sponsor::all();
    }
}
