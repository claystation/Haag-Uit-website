<?php

namespace HUplicatie\Http\Controllers\Website;

use Carbon\Carbon;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class LekkerBellenController extends Controller
{
    /**
     * LekkerBellenController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Lekkerbellen')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $kampjaar = Kampjaar::getActiveKampjaar();
        $inschrijvingen = $kampjaar->kampinschrijvingen()->with('persoon')->where('presentie', '!=', 'afgemeld')->get();
        $totaal = $inschrijvingen->count();

        $geslacht['man'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->geslacht === 'm';
        })->count();

        $geslacht['vrouw'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->geslacht === 'v';
        })->count();

        $geslacht['non-binair'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->geslacht === 'nb';
        })->count();

        $opleiding['cmd'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->studie->naam === 'CMD';
        })->count();

        $opleiding['hbo-ict'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->studie->naam === 'HBO-ICT';
        })->count();

        $opleiding['ads-ai'] = $inschrijvingen->filter(function ($inschrijving) {
            return $inschrijving->persoon->studie->naam === 'ADS&AI';
        })->count();

        $leeftijd['meerderjarig'] = $inschrijvingen->filter(function ($inschrijving) use ($kampjaar) {
            return $inschrijving->persoon->geboortedatum->diffInYears(Carbon::parse($kampjaar->start)) >= 18;
        })->count();
        $leeftijd['minderjarig'] = $inschrijvingen->filter(function ($inschrijving) use ($kampjaar) {
            return $inschrijving->persoon->geboortedatum->diffInYears(Carbon::parse($kampjaar->start)) < 18;
        })->count();

        $vorigKampjaar = Kampjaar::find($kampjaar->jaar - 1);
        $inschrijvingenVorigJaar = $vorigKampjaar->kampInschrijvingen()->where('presentie', '!=', 'afgemeld')->get();
        $totaalVorigJaar = $inschrijvingenVorigJaar->count();
        $totaalVorigJaarVandaag = $inschrijvingenVorigJaar->filter(function ($inschrijving) {
            return $inschrijving->created_at->lessThanOrEqualTo(Carbon::now()->subYear());
        })->count();

        return view(
            'website.lekkerbellen.index',
            compact([
                'kampjaar',
                'totaal',
                'geslacht',
                'opleiding',
                'leeftijd',
                'totaalVorigJaar',
                'totaalVorigJaarVandaag',
            ])
        );
    }
}
