<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Statistics;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Http\Request;

class RegistrationStatisticsController extends Controller
{
    public function registrationStatistics(Kampjaar $kampjaar): array
    {
        $date = Carbon::createFromDate($kampjaar->jaar);
        $period = new CarbonPeriod(
            $date->copy()->startOfYear(),
            '1 month',
            Carbon::parse($kampjaar->eind)->endOfMonth()
        );
        $result = [];

        $inschrijvingen = KampInschrijving::select('id', 'created_at')
            ->where('jaar', $kampjaar->jaar)
            ->where('presentie', '!=', 'afgemeld')
            ->get();
        $result['year'] = $kampjaar->jaar;
        $result['total'] = $inschrijvingen->count();
        foreach ($period->toArray() as $month) {
            $result['monthly'][$month->format('m')] = $inschrijvingen->filter(function ($inschrijving) use ($month) {
                return $inschrijving->created_at->lessThanOrEqualTo($month->copy()->endOfMonth());
            })->count();
        }

        return $result;
    }
}
