<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Actiepunt;

use HUplicatie\Authorization\Roles;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\User;

class ActiepuntenUsersController extends Controller
{
    /**
     * ActiepuntenUsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:Create Actiepunt|Edit Actiepunt')->only(['index']);
    }

    /**
     * Get all users assignable to actiepunt.
     *
     * @return mixed
     */
    public function index()
    {
        return User::role(Roles::STAFFER)->get(['id', 'name']);
    }
}
