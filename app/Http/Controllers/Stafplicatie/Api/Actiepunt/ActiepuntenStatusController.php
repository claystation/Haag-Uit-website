<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Actiepunt;

use Exception;
use HUplicatie\Actiepunt;
use HUplicatie\Http\Controllers\Controller;

class ActiepuntenStatusController extends Controller
{
    /**
     * ActiepuntenUsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:Edit Actiepunt')->only(['store', 'destroy']);
    }

    /**
     * Mark actiepunt as not done.
     *
     * @param  int  $id
     */
    public function store(int $id): void
    {
        $actiepunt = Actiepunt::withTrashed()->findOrFail($id);
        $actiepunt->restore();
    }

    /**
     * Mark actiepunt as done.
     *
     * @param  Actiepunt  $actiepunt
     *
     * @throws Exception
     */
    public function destroy(Actiepunt $actiepunt): void
    {
        $actiepunt->delete();
    }
}
