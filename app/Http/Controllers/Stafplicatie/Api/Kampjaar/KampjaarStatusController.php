<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Kampjaar;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;

class KampjaarStatusController extends Controller
{
    /**
     * KampjaarStatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:Edit Kampjaar')->only('store');
    }

    public function store(Kampjaar $kampjaar): void
    {
        $kampjaar->activate();
    }
}
