<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Inschrijving;

use HUplicatie\Filters\InschrijvingFilter;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;

class InschrijvingQueryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:View Inschrijvingen')->only('index');
    }

    public function index(InschrijvingFilter $filter)
    {
        return Kampjaar::active()
            ->first()
            ->kampInschrijvingen()
            ->filter($filter)
            ->with('persoon', 'latestBetaling')
            ->get()
            ->sortBy('persoon.voornaam')
            ->values()
            ->all();
    }
}
