<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Inschrijving;

use HUplicatie\Betaling;
use HUplicatie\Http\Controllers\Controller;
use Response;

class InschrijvingPaymentStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Edit Inschrijving')->only(['open', 'paid']);
    }

    public function open(Betaling $betaling)
    {
        if ($betaling->isIDeal()) {
            return Response::json('Dit is geen incasso betaling', 400);
        }
        $betaling->update(['status' => 'open']);

        return response('', 200);
    }

    public function paid(Betaling $betaling)
    {
        if ($betaling->isIDeal()) {
            return Response::json('Dit is geen incasso betaling', 400);
        }
        $betaling->update(['status' => 'paid']);

        return response('', 200);
    }
}
