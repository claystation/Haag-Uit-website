<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use Exception;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Storage;

class KampjaarController extends Controller
{
    /**
     * KampjaarController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Kampjaren')->only('index');
        $this->middleware('permission:View Kampjaar')->only('show');
        $this->middleware('permission:Create Kampjaar')->only(['create', 'store']);
        $this->middleware('permission:Edit Kampjaar')->only(['edit', 'update']);
        $this->middleware('permission:Delete Kampjaar')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $kampjaren = Kampjaar::orderBy('jaar', 'desc')->paginate(20);

        return view('stafplicatie.kampjaar.index', compact('kampjaren'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('stafplicatie.kampjaar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'jaar'              => 'required|unique:kampjaren|date_format:"Y"',
            'start'             => 'required|date',
            'eind'              => 'required|date',
            'inschrijfbedrag'   => 'required|numeric',
            'verzekeringbedrag' => 'required|numeric',
            'logo'              => 'image',
        ]);
        Kampjaar::create([
            'jaar'              => $request->get('jaar'),
            'start'             => $request->get('start'),
            'eind'              => $request->get('eind'),
            'inschrijfbedrag'   => $request->get('inschrijfbedrag'),
            'verzekeringbedrag' => $request->get('verzekeringbedrag'),
            'logo'              => $request->has('logo') ? $request->file('logo')->store('logos', 'public') : '',
        ]);

        return redirect('/stafplicatie/kampjaar')->with('flash', 'Kampjaar aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  Kampjaar  $kampjaar
     * @return Factory|View
     */
    public function show(Kampjaar $kampjaar)
    {
        return view('stafplicatie.kampjaar.show')
            ->with('kampjaar', $kampjaar);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Kampjaar  $kampjaar
     * @return Factory|View
     */
    public function edit(Kampjaar $kampjaar)
    {
        return view('stafplicatie.kampjaar.edit')
            ->with('kampjaar', $kampjaar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Kampjaar  $kampjaar
     * @return RedirectResponse
     */
    public function update(Request $request, Kampjaar $kampjaar): RedirectResponse
    {
        $request->validate([
            'start'             => 'required|date',
            'eind'              => 'required|date',
            'inschrijfbedrag'   => 'required|numeric',
            'verzekeringbedrag' => 'required|numeric',
            'logo'              => 'image',
        ]);
        $data = $request->only('start', 'eind', 'inschrijfbedrag', 'verzekeringbedrag');
        if ($request->has('logo')) {
            $data['logo'] = $request->file('logo')->store('logos', 'public');
        }

        $kampjaar->update($data);
        Storage::disk('public')->delete($kampjaar->logo);

        return redirect('/stafplicatie/kampjaar')->with('flash', 'Kampjaar gewijzigd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Kampjaar  $kampjaar
     * @return ResponseFactory|RedirectResponse|Response|Redirector
     *
     *@throws Exception
     */
    public function destroy(Kampjaar $kampjaar)
    {
        if ($kampjaar->actief) {
            return response('Een actief kampjaar mag niet verwijderd worden', 400);
        }
        $kampjaar->delete();
        if (\request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/stafplicatie/kampjaar');
    }
}
