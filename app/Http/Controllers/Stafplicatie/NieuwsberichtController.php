<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Nieuwsbericht;
use Illuminate\Http\Request;

class NieuwsberichtController extends Controller
{
    /**
     * NieuwsberichtController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Nieuwsberichten')->only('index');
        $this->middleware('permission:View Nieuwsbericht')->only('show');
        $this->middleware('permission:Create Nieuwsbericht')->only(['create', 'store']);
        $this->middleware('permission:Edit Nieuwsbericht')->only(['edit', 'update']);
        $this->middleware('permission:Delete Nieuwsbericht')->only('destroy');
    }

    public function index()
    {
        $nieuwsberichten = Nieuwsbericht::latest()->paginate(10);

        return view('stafplicatie.nieuwsbericht.index', compact(['nieuwsberichten']));
    }

    public function show(Nieuwsbericht $nieuwsbericht)
    {
        return view('stafplicatie.nieuwsbericht.show')
            ->with('nieuwsbericht', $nieuwsbericht);
    }

    public function create()
    {
        return view('stafplicatie.nieuwsbericht.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'titel' => 'required|max:60',
            'tekst' => 'required',
        ]);

        Nieuwsbericht::create($request->only(['titel', 'tekst']));

        return redirect('/stafplicatie/nieuwsbericht')->with('flash', 'Nieuwsbericht gepubliceerd');
    }

    public function update(Request $request, Nieuwsbericht $nieuwsbericht): void
    {
        $request->validate([
            'titel' => 'required|max:60',
            'tekst' => 'required',
        ]);

        $nieuwsbericht->update($request->only(['titel', 'tekst']));
    }

    public function destroy(Nieuwsbericht $nieuwsbericht)
    {
        $nieuwsbericht->delete();
        if (\request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/stafplicatie/nieuwsbericht');
    }
}
