<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Dashboard');
    }

    public function index()
    {
        return view('stafplicatie.dashboard.index');
    }
}
