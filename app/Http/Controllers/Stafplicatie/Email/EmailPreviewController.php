<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Email;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\IdealBetaling;
use HUplicatie\IncassoBetaling;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\ContactRequestMailable;
use HUplicatie\Mail\Website\InschrijvingPersoonMailable;
use HUplicatie\Mail\Website\InschrijvingStafMailable;
use HUplicatie\Persoon;
use Mollie\Api\MollieApiClient;
use Mollie\Api\Resources\Payment;
use stdClass;

/**
 * Class EmailPreviewController.
 *
 * @codeCoverageIgnore
 */
class EmailPreviewController extends Controller
{
    private $inschrijving;
    private $idealBetaling;
    private $incassoBetaling;

    /**
     * EmailPreviewController constructor.
     */
    public function __construct()
    {
        $this->inschrijving = new KampInschrijving(['id' => 1]);
        $this->inschrijving->persoon()->associate(new Persoon([
            'roepnaam'      => 'Petertje Metertje',
            'voornaam'      => 'Peter',
            'achternaam'    => 'Meter',
            'email'         => 'peter@meter.nl',
            'geslacht'      => 'm',
            'geboortedatum' => '2000-07-15',
        ]));
        $this->inschrijving->kampjaar()->associate(new Kampjaar(['jaar' => 2019]));
        $payment = new Payment(new MollieApiClient());
        $payment->id = 'tr_ikbenlit';
        $payment->amount = new stdClass();
        $payment->amount->value = '100.0';
        $this->idealBetaling = new IdealBetaling($payment);
        $this->idealBetaling->paymentUrl = 'https://www.mollie.com/payscreen/select-method/7UhSN1zuXS';

        $this->incassoBetaling = new IncassoBetaling('20190412123301', '100.00');
    }

    public function contactMail(): ContactRequestMailable
    {
        return new ContactRequestMailable(
            'Baccanardo',
            'bacc@nardo.nl',
            "Hoi staf, \r\n \r\n mag ik alvast 100 meter bestellen. \r\n \r\n kusjes"
        );
    }

    public function inschrijvingPersoonIdealMail(): InschrijvingPersoonMailable
    {
        $this->inschrijving->setRelation('betaling', $this->idealBetaling);

        return new InschrijvingPersoonMailable($this->inschrijving);
    }

    public function inschrijvingPersoonIncassoMail(): InschrijvingPersoonMailable
    {
        $this->inschrijving->setRelation('betaling', $this->incassoBetaling);

        return new InschrijvingPersoonMailable($this->inschrijving);
    }

    public function inschrijvingIdealStafMail(): InschrijvingStafMailable
    {
        $this->inschrijving->setRelation('betaling', $this->idealBetaling);

        return new InschrijvingStafMailable($this->inschrijving);
    }

    public function inschrijvingIncassoStafMail(): InschrijvingStafMailable
    {
        $this->inschrijving->setRelation('betaling', $this->incassoBetaling);

        return new InschrijvingStafMailable($this->inschrijving);
    }
}
