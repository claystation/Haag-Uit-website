<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use Exception;
use Hash;
use HUplicatie\Authorization\Roles;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Persoon;
use HUplicatie\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class PersoonController extends Controller
{
    /**
     * PersonController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Personen')->only('index');
        $this->middleware('permission:View Persoon')->only('show');
        $this->middleware('permission:Edit Persoon')->only(['edit', 'update']);
        $this->middleware('permission:Delete Persoon')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $personen = Persoon::orderBy('voornaam')->paginate(50);

        return view('stafplicatie.personen.index')->with('personen', $personen);
    }

    /**
     * Display the specified resource.
     *
     * @param  Persoon  $persoon
     * @return Factory|View
     */
    public function show(Persoon $persoon)
    {
        $persoon = $persoon->load([
            'kampInschrijving',
            'medewerkerInschrijvingen',
            'medewerkerInschrijvingen.eerste_keus',
            'medewerkerInschrijvingen.tweede_keus',
        ]);

        return view('stafplicatie.personen.show')
            ->with('persoon', $persoon);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return Factory|View
     */
    public function edit(User $user)
    {
        $roles = Role::all()->filter(function ($value) {
            return $value->name !== Roles::WEBMEESTER;
        });

        return view('stafplicatie.users.edit')
            ->with('user', $user)
            ->with('roles', $roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user): RedirectResponse
    {
        $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
                'max:255',
            ],
            'password' => 'sometimes|nullable|string|min:6|confirmed',
            'role'     => 'required|exists:roles,name|not_in:Webmeester',
        ]);
        $user->update([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => $request->has('password') ? Hash::make($request->get('password')) : $user->password,
        ]);
        $user->syncRoles($request->get('role'));

        return redirect('/stafplicatie/user')->with('flash', 'Gebruiker gewijzigd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return ResponseFactory|RedirectResponse|Response|Redirector
     *
     * @throws Exception
     */
    public function destroy(User $user)
    {
        if ($user->id === 1) {
            if (\request()->wantsJson()) {
                return response([], 403);
            }

            return redirect()->back()->with('flash', 'Nee sorry...');
        }
        $user->delete();
        if (\request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/stafplicatie/user');
    }
}
