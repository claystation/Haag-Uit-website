<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Aanwezigheid;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;
use Illuminate\Http\Request;

class AanwezigheidController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Edit Medewerker')->only(['update']);
    }

    public function index()
    {
        $medewerkers = Kampjaar::getActiveKampjaar()
            ->medewerkers()
            ->with('persoonsgegevens:id,voornaam,achternaam', 'aanwezigheid')
            ->select('id', 'persoon_id')
            ->get();

        return view('stafplicatie.aanwezigheid.index', compact(['medewerkers']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Aanwezigheid  $aanwezigheid
     * @return Aanwezigheid
     */
    public function update(Request $request, Aanwezigheid $aanwezigheid): Aanwezigheid
    {
        $request->validate([
            'status' => 'present|integer|min:0|max:2|nullable',
        ]);

        $aanwezigheid->update($request->only('status'));

        return $aanwezigheid;
    }
}
