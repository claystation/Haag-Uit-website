<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Pagina;

class PaginaController extends Controller
{
    /**
     * PaginaController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Paginas')->only('index');
        $this->middleware('permission:View Pagina')->only('show');
        $this->middleware('permission:Create Pagina')->only(['create', 'store']);
        $this->middleware('permission:Edit Pagina')->only(['edit', 'update']);
        $this->middleware('permission:Delete Pagina')->only('destroy');
    }

    public function index()
    {
        $paginas = Pagina::all();

        return view('stafplicatie.pagina.index', compact('paginas'));
    }

    public function show(Pagina $pagina)
    {
        return view('stafplicatie.pagina.show')->with('pagina', $pagina);
    }

    public function edit(Pagina $pagina)
    {
        return view('stafplicatie.pagina.edit')->with('pagina', $pagina);
    }
}
