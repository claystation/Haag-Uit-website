<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Actiepunt;
use HUplicatie\Authorization\Roles;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;
use HUplicatie\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class ActiepuntController extends Controller
{
    /**
     * ActiepuntController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Actiepunten')->only('index');
        $this->middleware('permission:View Actiepunt')->only('show');
        $this->middleware('permission:Create Actiepunt')->only(['create', 'store']);
        $this->middleware('permission:Edit Actiepunt')->only(['edit', 'update']);
        $this->middleware('permission:Delete Actiepunt')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $actiepunten = Actiepunt::withTrashed()->activeYear()->get();

        return view('stafplicatie.actiepunt.index', compact('actiepunten'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return ResponseFactory|Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'beschrijving'       => 'required|string',
            'deadline'           => 'filled|date',
            'assignedUsers'      => 'required',
            'assignedUsers.*.id' => [
                'required',
                'exists:users,id',
                Rule::in(User::select('id')->role(Roles::STAFFER)->get()
                    ->map(function ($value) {
                        return $value->id;
                    })->all()),
            ],
        ]);

        $ap = Actiepunt::create([
            'beschrijving' => $request->get('beschrijving'),
            'deadline'     => $request->get('deadline'),
            'jaar'         => Kampjaar::getActiveKampjaar()->jaar,
        ]);
        $ap->users()->sync(Collection::make($request->get('assignedUsers'))->pluck('id'));

        return response($ap, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $actiepunt
     * @return Actiepunt
     */
    public function show(int $actiepunt): Actiepunt
    {
        return Actiepunt::withTrashed()->findOrFail($actiepunt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $actiepunt
     * @return Actiepunt|Actiepunt[]|Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Builder[]|Model
     */
    public function update(Request $request, int $actiepunt)
    {
        $ap = Actiepunt::withTrashed()->findOrFail($actiepunt);

        $request->validate([
            'beschrijving'       => 'required|string',
            'deadline'           => 'filled|date',
            'assignedUsers'      => 'required',
            'assignedUsers.*.id' => [
                'required',
                'exists:users,id',
                Rule::in(User::select('id')->role(Roles::STAFFER)->get()
                    ->map(function ($value) {
                        return $value->id;
                    })->all()),
            ],
        ]);

        $ap->update($request->only(['beschrijving', 'deadline']));
        $ap->users()->sync(Collection::make($request->get('assignedUsers'))->pluck('id'));

        return $ap;
    }
}
