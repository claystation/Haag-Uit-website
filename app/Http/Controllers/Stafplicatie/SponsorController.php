<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Sponsor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class SponsorController extends Controller
{
    /**
     * SponsorController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Sponsoren')->only('index');
        $this->middleware('permission:Create Sponsor')->only(['create', 'store']);
        $this->middleware('permission:Edit Sponsor')->only(['edit', 'update', 'restore', 'remove']);
        $this->middleware('permission:Delete Sponsor')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $sponsoren = Sponsor::withTrashed()->paginate(20);

        return view('stafplicatie.sponsor.index', compact('sponsoren'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('stafplicatie.sponsor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'naam' => 'required',
            'url'  => 'required',
            'logo' => 'required|image',
        ]);

        Sponsor::create([
            'naam' => $request->get('naam'),
            'url'  => $request->get('url'),
            'logo' => $request->file('logo')->store('sponsoren', 'public'),
        ]);

        return redirect('/stafplicatie/sponsor')->with('flash', 'Sponsor aangemaakt');
    }

    /**
     * Restore a resource in storage.
     *
     * @param  int  $sponsor
     * @return void
     */
    public function restore(int $sponsor): void
    {
        Sponsor::withTrashed()->findOrFail($sponsor)->restore();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Sponsor  $sponsor
     * @return Application|Factory|Response|View
     */
    public function edit(Sponsor $sponsor)
    {
        return view('stafplicatie.sponsor.edit')->with('sponsor', $sponsor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Sponsor  $sponsor
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function update(Request $request, Sponsor $sponsor)
    {
        $request->validate([
            'naam' => 'required',
            'url'  => 'required',
            'logo' => 'image',
        ]);

        $data = $request->only('naam', 'url');
        if ($request->has('logo')) {
            $data['logo'] = $request->file('logo')->store('sponsoren', 'public');
        }

        $sponsor->update($data);

        return redirect('/stafplicatie/sponsor')->with('flash', 'Sponsor gewijzigd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return void
     */
    public function destroy(int $id): void
    {
        $sponsor = Sponsor::onlyTrashed()->findOrFail($id);
        $sponsor->forceDelete();
    }

    public function remove(Sponsor $sponsor): void
    {
        $sponsor->delete();
    }
}
