<?php

namespace HUplicatie\Http\Middleware;

use Closure;
use HUplicatie\Kampjaar;
use Illuminate\Http\Request;

class RegistrationOpen
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Kampjaar::getOpenKampjaar()) {
            return redirect('/inschrijven/gesloten');
        }

        return $next($request);
    }
}
