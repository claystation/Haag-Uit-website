<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Betaling.
 *
 * @property string $transactie_id
 */
class Betaling extends Model
{
    protected $table = 'betalingen';

    protected $guarded = ['type'];

    public $paymentUrl;

    public function getBedragAttribute($value): string
    {
        return number_format($value, 2);
    }

    public function setBedragAttribute($value): void
    {
        $this->attributes['bedrag'] = (float) $value;
    }

    public function isIncasso(): bool
    {
        return $this->type === 'incasso';
    }

    public function isIDeal(): bool
    {
        return $this->type === 'ideal';
    }

    public function isOpen(): bool
    {
        return $this->status === 'open';
    }

    public function isPaid(): bool
    {
        return $this->status === 'paid';
    }

    public function isExpired(): bool
    {
        return $this->status === 'expired';
    }

    public function isFailed(): bool
    {
        return $this->status === 'failed';
    }

    public function isCanceled(): bool
    {
        return $this->status === 'canceled';
    }

    /**
     * Betaling belongs to a KampInschrijving.
     *
     * @return BelongsTo
     */
    public function kampInschrijving(): BelongsTo
    {
        return $this->belongsTo(KampInschrijving::class);
    }
}
