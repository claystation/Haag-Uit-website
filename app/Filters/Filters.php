<?php

namespace HUplicatie\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Filters.
 */
abstract class Filters
{
    /**
     * Array of filters.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Request.
     *
     * @var Request
     */
    protected $request;

    /**
     * Builder.
     *
     * @var Builder
     */
    protected $builder;

    /**
     * Filters constructor.
     *
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply filters.
     *
     * @param  Builder  $builder
     * @return mixed
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
     * Get filters.
     *
     * @return array
     */
    private function getFilters(): array
    {
        return $this->request->only($this->filters);
    }
}
