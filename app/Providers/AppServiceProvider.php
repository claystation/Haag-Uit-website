<?php

namespace HUplicatie\Providers;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (! $this->app->environment('production') && ! $this->app->environment('staging')) {
            $this->app->singleton(Generator::class, function () {
                return Factory::create('nl_NL');
            });
        }
    }
}
