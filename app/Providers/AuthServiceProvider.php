<?php

namespace HUplicatie\Providers;

use HUplicatie\Authorization\Roles;
use HUplicatie\Policies\UserPolicy;
use HUplicatie\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->hasRole(Roles::WEBMEESTER)) {
                return true;
            }
        });
    }
}
