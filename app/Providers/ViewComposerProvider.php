<?php

namespace HUplicatie\Providers;

use HUplicatie\Http\ViewComposers\KampjaarComposer;
use HUplicatie\Http\ViewComposers\QuoteComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        View::composer(
            ['website.layouts.website'], QuoteComposer::class
        );

        View::composer(
            ['website.*'], KampjaarComposer::class
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
