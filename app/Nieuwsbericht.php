<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Nieuwsbericht extends Model
{
    protected $table = 'nieuwsberichten';

    protected $fillable = ['titel', 'tekst'];

    /**
     * Returns a preview of the text.
     *
     * @return string
     */
    public function previewText(): string
    {
        return Str::limit($this->tekst);
    }
}
