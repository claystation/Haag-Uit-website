<?php

namespace HUplicatie\Events;

use HUplicatie\KampInschrijving;
use Illuminate\Queue\SerializesModels;

class RegistrationSuccessEvent
{
    use SerializesModels;

    /**
     * Kamp Inschrijving.
     *
     * @var KampInschrijving
     */
    public $inschrijving;
    public $paymentUrl;

    /**
     * Create a new event instance.
     *
     * @param  KampInschrijving  $inschrijving
     * @param  string|null  $paymentUrl
     */
    public function __construct(KampInschrijving $inschrijving, string $paymentUrl = null)
    {
        $this->inschrijving = $inschrijving;
        $this->paymentUrl = $paymentUrl;
    }
}
