<?php

namespace HUplicatie;

class IncassoBetaling extends Betaling
{
    /**
     * IncassoBetaling constructor.
     *
     * @param  string  $transactieId
     * @param  string  $amount
     */
    public function __construct(string $transactieId, string $amount)
    {
        parent::__construct();
        $this->attributes['type'] = 'incasso';
        $this->transactie_id = $transactieId;
        $this->bedrag = $amount;
    }

    public function setRekeninghouderNaam(string $rekeninghouderNaam): IncassoBetaling
    {
        $this->rekeninghouder_naam = $rekeninghouderNaam;

        return $this;
    }

    public function setRekeninghouderNummer(string $rekeninghouderNummer): IncassoBetaling
    {
        $this->rekeninghouder_nummer = $rekeninghouderNummer;

        return $this;
    }

    public function setRekeninghouderPlaats(string $rekeninghouderWoonplaats): IncassoBetaling
    {
        $this->rekeninghouder_woonplaats = $rekeninghouderWoonplaats;

        return $this;
    }
}
