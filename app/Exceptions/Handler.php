<?php

namespace HUplicatie\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  Exception  $exception
     * @return void
     *
     *@throws Exception
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception) && ! app()->environment('testing')) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param  Exception  $exception
     * @return JsonResponse|\Illuminate\Http\Response|Response
     *
     * @throws Exception
     */
    public function render($request, \Exception $exception)
    {
        if ($request->ajax() || $request->wantsJson()) {
            if ($exception instanceof ValidationException) {
                return response('De ingevulde data is incorrect', 422);
            }

            return parent::render($request, $exception);
        }

        if ($this->shouldReport($exception) && ! $this->isHttpException($exception) && ! config('app.debug')) {
            $exception = new HttpException(500, 'Whoops!');
        }
        if ($this->isHttpException($exception)) {
            $status = $exception->getStatusCode();

            $basePath = $request->is('stafplicatie', 'stafplicatie/**') ? 'stafplicatie' : 'website';
            if (view()->exists("{$basePath}/errors/{$status}")) {
                return response()->view("{$basePath}/errors/{$status}", ['exception' => $exception], $status);
            }
        }

        return parent::render($request, $exception);
    }
}
