<?php

namespace HUplicatie\Services\Betalingen;

use HUplicatie\Betaling;
use HUplicatie\Exceptions\IdealBetalingCreationException;
use HUplicatie\IdealBetaling;
use Mollie\Api\Exceptions\ApiException;

class IdealBetalingService extends BetalingService
{
    /**
     * Process the iDeal betaling.
     *
     * @param  string  $amount
     * @return Betaling
     *
     * @throws IdealBetalingCreationException
     */
    public function processBetaling(string $amount): Betaling
    {
        $mollieService = new MollieService();

        try {
            $payment = $mollieService->createPaymentForInschrijving($amount, $this->inschrijving);
            $betaling = new IdealBetaling($payment);
            $this->inschrijving->addBetaling($betaling);
            $betaling->paymentUrl = $payment->getCheckoutUrl();

            return $betaling;
        } catch (ApiException $exception) {
            throw new IdealBetalingCreationException(
                "Er is iets mis gegaan bij het aanmaken van inschrijving met id {$this->inschrijving->id}",
                0,
                $exception
            );
        }
    }
}
