<?php

namespace HUplicatie\Services\Betalingen;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;

abstract class BetalingService
{
    /**
     * Kampinschrijving.
     *
     * @var KampInschrijving
     */
    protected $inschrijving;

    /**
     * BetalingService constructor.
     *
     * @param  KampInschrijving  $inschrijving
     */
    public function __construct(KampInschrijving $inschrijving)
    {
        $this->inschrijving = $inschrijving;
    }

    abstract public function processBetaling(string $amount): Betaling;
}
