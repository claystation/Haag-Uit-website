<?php

namespace HUplicatie\Services\Betalingen;

use HUplicatie\KampInschrijving;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\Resources\Payment;
use Mollie\Laravel\Facades\Mollie;

class MollieService
{
    /**
     * Create a payment for Mollie.
     *
     * @param  string  $amount
     * @param  string  $description
     * @param  string  $returnUrl
     * @return Payment
     *
     * @throws ApiException
     */
    public function createPayment(string $amount, string $description, string $returnUrl): Payment
    {
        return Mollie::api()->payments()->create([
            'amount'      => [
                'currency' => 'EUR',
                'value'    => $amount,
            ],
            'redirectUrl' => $returnUrl,
            'description' => $description,
            'webhookUrl'  => route('paymentsWebhook'),
        ]);
    }

    /**
     * Create a payment for an inschrijving for Mollie.
     *
     * @param  string  $amount
     * @param  KampInschrijving  $inschrijving
     * @return Payment
     *
     * @throws ApiException
     */
    public function createPaymentForInschrijving(string $amount, KampInschrijving $inschrijving): Payment
    {
        return $this->createPayment(
            $amount,
            "Betaling introductieweek Haag Uit {$inschrijving->kampjaar->jaar}",
            route('returnUrl', ['id' => $inschrijving->id])
        );
    }
}
