<?php

namespace HUplicatie\Services\Betalingen;

use Carbon\Carbon;
use HUplicatie\Betaling;
use HUplicatie\IncassoBetaling;
use HUplicatie\KampInschrijving;

class IncassoBetalingService extends BetalingService
{
    private $transactie_id;
    private $incassoData;

    /**
     * IncassoBetalingService constructor.
     *
     * @param  KampInschrijving  $inschrijving
     * @param  array  $incassoData
     */
    public function __construct(KampInschrijving $inschrijving, array $incassoData)
    {
        parent::__construct($inschrijving);
        $this->transactie_id = Carbon::now()->format('YmdHis').$this->inschrijving->id;
        $this->incassoData = $incassoData;
    }

    /**
     * Process the incasso betaling.
     *
     * @param  string  $amount
     * @return Betaling
     */
    public function processBetaling(string $amount): Betaling
    {
        $incassoBetaling = new IncassoBetaling($this->transactie_id, $amount);
        $incassoBetaling->setRekeninghouderNaam($this->incassoData['rekeninghouder_naam'])
            ->setRekeninghouderNummer($this->incassoData['rekeninghouder_nummer'])
            ->setRekeninghouderPlaats($this->incassoData['rekeninghouder_woonplaats']);
        $this->inschrijving->addBetaling($incassoBetaling);

        return $incassoBetaling;
    }
}
