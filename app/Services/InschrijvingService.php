<?php

namespace HUplicatie\Services;

use Exception;
use HUplicatie\Exceptions\InschrijvingCreationException;
use HUplicatie\Http\Requests\Website\RegistratieRequest;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Persoon;
use Log;

class InschrijvingService
{
    /**
     * Kampjaar.
     *
     * @var Kampjaar
     */
    private $kampjaar;

    /**
     * InschrijvingService constructor.
     *
     * @param  Kampjaar  $kampjaar
     */
    public function __construct(Kampjaar $kampjaar)
    {
        $this->kampjaar = $kampjaar->active()->first();
    }

    /**
     * Process the inschrijving.
     *
     * @param  RegistratieRequest  $request
     * @return KampInschrijving
     *
     * @throws InschrijvingCreationException
     */
    public function processInschrijving(RegistratieRequest $request): ?KampInschrijving
    {
        try {
            return $this->createInschrijving($request);
        } catch (Exception $exception) {
            Log::warning("Exception creating inschrijving: {$exception->getMessage()}");
            throw new InschrijvingCreationException('Het is niet gelukt de inschrijving aan te maken.', 0, $exception);
        }
    }

    /**
     * Create the inschrijving.
     *
     * @param  RegistratieRequest  $request
     * @return KampInschrijving
     */
    private function createInschrijving(RegistratieRequest $request): KampInschrijving
    {
        return $this->createKampInschrijving($request);
    }

    /**
     * Create persoon for inschrijving.
     *
     * @param  RegistratieRequest  $request
     * @return Persoon
     */
    private function createPersoon(RegistratieRequest $request): Persoon
    {
        return Persoon::create($request->only([
            'roepnaam',
            'voornaam',
            'voorletters',
            'achternaam',
            'geboortedatum',
            'geslacht',
            'opleiding',
            'straat',
            'huisnummer',
            'postcode',
            'woonplaats',
            'email',
            'mobiel',
            'studie_id',
        ]));
    }

    /**
     * Create and associate the kamp inschrijving.
     *
     * @param  RegistratieRequest  $request
     * @return KampInschrijving
     */
    private function createKampInschrijving(RegistratieRequest $request): KampInschrijving
    {
        $inschrijvingData = [];
        $inschrijvingData['opmerking'] = $request->input('opmerking');
        $inschrijvingData['kledingmaat'] = $request->input('kledingmaat', '');
        $inschrijvingData['telefoon_nood'] = $request->input('telefoon_nood', '');
        $inschrijvingData['annuleringsverzekering'] = $request->input('annuleringsverzekering', '0');
        $kampInschrijving = (new KampInschrijving($inschrijvingData));
        $kampInschrijving->kampjaar()->associate($this->kampjaar);
        $kampInschrijving->persoon()->associate($this->createPersoon($request));
        $kampInschrijving->save();

        return $kampInschrijving;
    }
}
