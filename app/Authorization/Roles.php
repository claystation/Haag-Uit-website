<?php

namespace HUplicatie\Authorization;

class Roles
{
    public const WEBMEESTER = 'Webmeester';
    public const STAFFER = 'Staffer';
    public const MEDEWERKER = 'Medewerker';
    public const INSCHRIJVING = 'Inschrijving';
}
