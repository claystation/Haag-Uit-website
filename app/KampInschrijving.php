<?php

namespace HUplicatie;

use HUplicatie\Filters\InschrijvingFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Log;

/**
 * Entity voor Kamp Inschrijving (Feut).
 *
 * @property int $id
 * @property Persoon $persoon
 * @property string $kledingmaat
 * @property string $telefoon_nood
 * @property string $opmerking
 * @property string $presentie
 * @property bool $annuleringsverzekering
 */
class KampInschrijving extends Model
{
    protected $table = 'kamp_inschrijvingen';

    protected $guarded = ['id'];

    public function addBetaling(Betaling $betaling): KampInschrijving
    {
        $this->betalingen()->save($betaling);
        Log::info("Created payment id: $betaling->transactie_id for Inschrijving: $this->id");

        return $this;
    }

    /**
     * Scope for filtering kamp inschrijvingen.
     *
     * @param  Builder  $query
     * @param  InschrijvingFilter  $filters
     * @return mixed
     */
    public function scopeFilter(Builder $query, InschrijvingFilter $filters)
    {
        return $filters->apply($query);
    }

    /**
     * Belongs to persoon.
     *
     * @return BelongsTo
     */
    public function persoon(): BelongsTo
    {
        return $this->belongsTo(Persoon::class);
    }

    /**
     * Has many betalingen.
     *
     * @return HasMany
     */
    public function betalingen(): HasMany
    {
        return $this->hasMany(Betaling::class, 'kamp_inschrijving_id');
    }

    /**
     * Has one latest betaling.
     *
     * @return HasOne
     */
    public function latestBetaling(): HasOne
    {
        return $this->hasOne(Betaling::class, 'kamp_inschrijving_id')->latest();
    }

    /**
     * Belongs to a kampjaar.
     *
     * @return BelongsTo
     */
    public function kampjaar(): BelongsTo
    {
        return $this->belongsTo(Kampjaar::class, 'jaar', 'jaar');
    }

    /**
     * Has one pakket.
     *
     * @return HasOne
     */
    public function pakket(): HasOne
    {
        return $this->hasOne(Pakket::class);
    }
}
