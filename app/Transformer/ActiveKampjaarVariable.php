<?php

namespace HUplicatie\Transformer;

use HUplicatie\Kampjaar;

class ActiveKampjaarVariable implements SubstituteVariable
{
    private $variable = '$JAAR$';
    private $replacement;

    /**
     * JaarVariable constructor.
     */
    public function __construct()
    {
        $this->replacement = Kampjaar::getActiveKampjaar()->jaar;
    }

    public function getVariable(): string
    {
        return $this->variable;
    }

    public function getReplacement(): string
    {
        return $this->replacement;
    }
}
