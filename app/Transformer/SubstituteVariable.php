<?php

namespace HUplicatie\Transformer;

interface SubstituteVariable
{
    public function getVariable(): string;

    public function getReplacement(): string;
}
