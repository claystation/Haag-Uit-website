<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Actiepunt extends Model
{
    use SoftDeletes;

    protected $table = 'actiepunten';

    protected $appends = ['assignedUsers'];

    protected $guarded = ['id'];

    /**
     * Scope to return actiepunten for active year.
     *
     * @param  Builder  $query
     * @return mixed
     */
    public function scopeActiveYear(Builder $query)
    {
        return $query->whereHas('kampjaar', function ($query) {
            return $query->where('actief', 1);
        });
    }

    /**
     * Add Assigned users as attribute.
     *
     * @return Collection
     */
    public function getAssignedUsersAttribute(): Collection
    {
        return $this->users()->get(['name', 'id']);
    }

    /**
     * Belongs to Kampjaar.
     *
     * @return BelongsTo
     */
    public function kampjaar(): BelongsTo
    {
        return $this->belongsTo(Kampjaar::class, 'jaar');
    }

    /**
     * Belongs to many users.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
