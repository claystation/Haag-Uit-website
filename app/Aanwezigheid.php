<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Aanwezigheid extends Model
{
    protected $guarded = ['id'];
    protected $table = 'aanwezigheden';

    public function medewerker(): BelongsTo
    {
        return $this->belongsTo(Medewerker::class);
    }
}
