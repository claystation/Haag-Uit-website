<?php

namespace HUplicatie\Mail\Website;

use HUplicatie\KampInschrijving;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InschrijvingStafMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Kamp Inschrijving.
     *
     * @var KampInschrijving
     */
    public $inschrijving;

    /**
     * Create a new message instance.
     *
     * @param  KampInschrijving  $inschrijving
     */
    public function __construct(KampInschrijving $inschrijving)
    {
        $this->inschrijving = $inschrijving;
        $this->from('staf@haaguit.com', 'HUplicatie')
            ->to('staf@haaguit.com', 'Staf Haag Uit')
            ->subject("Nieuwe inschrijving voor Haag Uit {$inschrijving->kampjaar->jaar}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->markdown('email.inschrijving.staf');
    }
}
