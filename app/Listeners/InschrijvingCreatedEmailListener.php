<?php

namespace HUplicatie\Listeners;

use HUplicatie\Events\RegistrationSuccessEvent;
use HUplicatie\Mail\Website\InschrijvingPersoonMailable;
use HUplicatie\Mail\Website\InschrijvingStafMailable;
use Illuminate\Contracts\Mail\Mailer;

class InschrijvingCreatedEmailListener
{
    /**
     * Mailer.
     *
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param  Mailer  $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationSuccessEvent  $event
     * @return void
     */
    public function handle(RegistrationSuccessEvent $event): void
    {
        $this->mailer->queue(new InschrijvingPersoonMailable($event->inschrijving, $event->paymentUrl));
        $this->mailer->queue(new InschrijvingStafMailable($event->inschrijving));
    }
}
